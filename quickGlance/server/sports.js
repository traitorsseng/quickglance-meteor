Sports = new Meteor.Collection("sports");

if(Meteor.isServer) {
    Meteor.methods({
        getSportScores: function() {
            Sports.remove({});
            var currentDate = moment().format("YYYY-MM-DD");
            console.log("Current Date: " + currentDate);
            var url = "https://raw.githubusercontent.com/openfootball/football.json/master/2016-17/en.1.json";
            var url1 = "";
            var url2 = "";
            var result = HTTP.call('GET', url);
            var results = JSON.parse(result.content);
            var md = "";
            var date = "";
            var response = [];
            //console.log(results);
            for(var i = 0; i < results.rounds.length; i++) {
                md = results.rounds[i];
                //date = md[0].matches.date;
                //console.log(md.matches[0].date);
                date = md.matches[0].date;
                //console.log(date);
                if(moment(currentDate).isBefore(date) || moment(currentDate).isSame(date)){
                    console.log("Match Day: " + date);
                    response.add(md);
                    break;
                }
            }
            Sports.insert(response);
            console.log(response);
        },
    });
}
