var array = [];
if(Meteor.isClient) {
    Template.filterBar.events ({
        'change .typenews' : function(event, template) {
            var selected = template.findAll("input[type=checkbox]:checked");
            var array = _.map(selected, function(item) {
                return item.defaultValue;
            });
            console.log("Current items in Type Array: " + array);
            var x = $(event.target).is(":checked");
            var value = $(event.target).val();

            if(x == true) {
                if (value == "All"){
                    array = [];
                    array.push("world");
                    array.push("business");
                    array.push("technology");
                    array.push("dining");
                    array.push("sports");
                }
                else{
                    console.log($(event.target).val() +" Checked");
                    //array.push($(event.target).val());
                }
            }
            else {
                console.log($(event.target).val() +" UnChecked");

                if (value == "All"){
                    array = [];
                }

                else{
                    // for(var i = 0; i < array.length ; i++) {
                    //     if(array[i] == $(event.target).val()) {
                    //         array.splice(i, 1);
                    //     }
                    // }
                }
            }

            console.log("User: " + Meteor.userId());
            console.log("Sending array: " + array);

            // get the user's updated preferences, these are the markers currently displayed
            Meteor.call("getPreferences", Meteor.userId(), addUserCallback(array));
        }
    });

    Template.filterBar.helpers ({
        isChecked: function(value) {
            // use a reactive call to return user preferences
            var results = ReactiveMethod.call("getPreferences", Meteor.userId());
            var serverPreferences;
            // get the local array of preferences
            if (typeof results == "undefined" || results.length == 0){
                serverPreferences = [];
            }
            else {
                serverPreferences = results;
            }
            //Check if the given type is a user preference
            for(var i = 0; i < serverPreferences.length; i++) {
                if(serverPreferences[i] == value) {
                    array.push(value);
                    return true;
                }
            }
            return false;
        }
    });
    Template.locationBar.helpers ({
        isChecked: function(value) {
            var result = ReactiveMethod.call("getLocationsToZoom", Meteor.userId());
            console.log("Users Locations: " + result);
            if(result == value) {
                return true;
            } else {
                return false;
            }
        }
    });
}

addUser = addUserCallback;

function getLocalPref(){
    return array;
}

function addUserCallback(array){
    var returnFunction = function(err, results){
        if (err){
            console.log("Error acquiring user's preferences.");
        }

        else{
            var serverPreferences;
            // get the local array of preferences
            if (typeof results == "undefined" || results.length == 0){
                serverPreferences = [];
            }

            else{
                serverPreferences = results;
            }
            // compare it with the local preference
            toAdd = whatToAdd(array, serverPreferences);
            toRemove = whatToRemove(array, serverPreferences);

            console.log("Local array: " + array);
            console.log("Server array: " + serverPreferences);
            console.log("Pref to add: " + toAdd);
            console.log("Pref to remove: " + toRemove);

            // add or remove certain articles only
            addMarkers(toAdd);
            removeMarkers(toRemove);

            // update the user's preferences
            // Meteor.call("newUser", Meteor.userId(), array);
            if (Meteor.userId()){
                Meteor.call("updatePreferences", Meteor.userId(), array);
            }
        }
    };
    return returnFunction;
}


// determine which preferences we need to add
function whatToAdd(localPreferences, serverPreferences){
    var toAdd = [];
    var j = 0;
    for (var i = 0; i < localPreferences.length; i++){
        if (serverPreferences.indexOf(localPreferences[i]) == -1){
            // a preference specified locally is currently not displayed
            toAdd[j++] = localPreferences[i];
        }
    }

    return toAdd;
}

// determine which preferences to remove
function whatToRemove(localPreferences, serverPreferences){
    var toRemove = [];
    var j = 0;
    for (var i = 0; i < serverPreferences.length; i++){
        if (localPreferences.indexOf(serverPreferences[i]) == -1){
            toRemove[j++] = serverPreferences[i];
        }
    }

    return toRemove;
}
