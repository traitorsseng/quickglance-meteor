Comments = new Meteor.Collection("comments");
Notifications = new Meteor.Collection("notifications");

if(Meteor.isClient) {
    Template.commentBox.onRendered(function () {
        Session.set('numChars', 0);
    });

    getChatNotifs();

    checkChatNotifs = getChatNotifs;

    function getChatNotifs() {
        var comms = Comments.find();
        comms.observeChanges({
            added: function(id, object) {

                console.log("Added object to Comments : " + object.a_id + " . URL: " + object.a_url);
                var article_id = object.a_id;
                console.log("User of new comment: " + object.user + " logged user: " + Meteor.userId());

                if(object.user != Meteor.userId()) {
                    setTimeout(function() {
                        Meteor.call("checkSubscribedArticles", Meteor.userId(), article_id, function (err, results) {
                            if(err) {
                                console.log("Error checking subscribed articles");
                            } else {
                                console.log("Is in subscribedArticles: " + results);
                                if(results == true) {
                                    var findResults = Notifications.find({
                                        user_id: Meteor.userId(),
                                        commenter_name: object.username,
                                        comment_message: object.message,
                                        time: object.timestamp,
                                        article_url: object.a_url,
                                        article_title: object.a_id
                                    }).fetch();
                                    console.log("Find Results: " + findResults.length);
                                    if(findResults.length == 0) {
                                        sAlert.success(object.username + ' has responded to your comment to ' + "<b>" + object.a_id + "</b>", {timeout: 5000, html: true});
                                        Notifications.insert({
                                            user_id: Meteor.userId(),
                                            commenter_name: object.username,
                                            comment_message: object.message,
                                            time: object.timestamp,
                                            article_url: object.a_url,
                                            article_title: object.a_id,
                                            read_notif: false
                                        });
                                    }
                                }
                            }
                        });
                    },0);
                }
            }
        });
    }

    Template.commentBox.events({
        'input #commentText' : function() {
            Session.set('numChars', $('#commentText').val().length);
        },
        'click button' : function() {
            var comment = $('#commentText').val();
            $('#commentText').val("");
            Session.set('numChars', 0);
            var article_id = Session.get('article_id');
            var article_url = Session.get('article_url');
            var userName;
            console.log(Meteor.user().services);
            if(Meteor.user().services.facebook != null) {
                userName = Meteor.user().services.facebook.name;
                profilePicture = "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/?type=square&height=160&width=160";
                console.log(Meteor.user().services.facebook.id);
            } else {
                userName = Meteor.user().services.google.name;
                profilePicture = Meteor.user().services.google.picture;
            }
            // once we remove the insecure package, this all breaks
            if(Meteor.user()) {
                Comments.insert({
                    message: comment,
                    a_id: article_id,
                    a_url: article_url,
                    user: Meteor.userId(),
                    username: userName,
                    picture: profilePicture,
                    timestamp: new Date()
                });

                Meteor.call("addToSubscribedArticles", Meteor.userId(), article_id);
            }
        },
        'keypress' : function(e, template) {
            if (e.which == 13 && $('#commentText').val().length > 0) {
                console.log('Pressed Enter');
                e.preventDefault();
                var comment = $('#commentText').val();
                $('#commentText').val("");
                Session.set('numChars', 0);
                var article_id = Session.get('article_id');
                var article_url = Session.get('article_url');
                var userName;
                console.log(Meteor.user().services);
                if(Meteor.user().services.facebook != null) {
                    userName = Meteor.user().services.facebook.name;
                    profilePicture = "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/?type=square&height=160&width=160";
                    console.log(Meteor.user().services.facebook.id);
                } else {
                    userName = Meteor.user().services.google.name;
                    profilePicture = Meteor.user().services.google.picture;
                }
                // once we remove the insecure package, this all breaks
                if(Meteor.user()) {
                    Comments.insert({
                        message: comment,
                        a_id: article_id,
                        a_url: article_url,
                        user: Meteor.userId(),
                        username: userName,
                        picture: profilePicture,
                        timestamp: new Date()
                    });

                    Meteor.call("addToSubscribedArticles", Meteor.userId(), article_id);
                }
            }
        }
    });

    Template.commentBox.helpers({
        charCount: function() {
            return 140 - Session.get('numChars');
        },

        charClass: function() {
            if(Session.get('numChars') > 140) {
                return 'errCharCount';
            } else {
                return 'charCount';
            }
        },

        disableButton: function() {
            if(Session.get('numChars') <= 0 || Session.get('numChars') > 140 || !Meteor.user()) {
                return 'disabled';
            }
        }
    });
}
