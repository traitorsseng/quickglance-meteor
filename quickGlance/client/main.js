// Send all the information about the user to the server when they closed the application
import { Meteor } from 'meteor/meteor'
Meteor.startup(function(){
    $(window).bind('beforeunload', function() {
        closingWindow();
        // return "Are you sure you want to leae";
    });

    // all the things to do when the browser starts
    console.log("[START UP CODE]: ");
    Session.set('locationZoom', 'World');


    Meteor.call("getBookmarks", Meteor.userId(), getBookmarksCallback());

    Meteor.call("getLocationsToZoom", Meteor.userId(), function(err, results){
        if (err){
            console.log("Error acquiring location to zoom into");
        }

        else{
            if (results == "" || typeof results == "undefined"){
                Session.set("locationZoom", "World");
            }

            else{
                Session.set("locationZoom", results);
            }
        }
    });
});

closingWindow = function(){
    Meteor.call("updateBookmarks", Meteor.userId(), Bookmarks.find().fetch());
    Meteor.call("updateLocationsToZoom", Meteor.userId(), Session.get('locationZoom'));
}

getBookmarksCallback = function(){
    var returnFunction = function(err, results){
        if (err){
            console.log("Error acquiring user bookmarks");
        }

        else{
            var userBookmarksServer = results;
            console.log("[CLIENT START UP]: Getting bookmarks");
            for (var i = 0; i < userBookmarksServer.length; i++){
                // userBookmarksServer[i]._id = new Meteor.Collection.ObjectID();
                var bookmark = userBookmarksServer[i];
                var exists = Bookmarks.find({_id: bookmark._id}).fetch();
                console.log("Bookmark: " + bookmark + " Exists = " + exists[0]);
                if (typeof exists[0] == "undefined"){
                    // if (exists[0]._id != bookmark._id){
                    console.log("Adding article to database of bookmarks");
                    Bookmarks.insert(bookmark);
                    //}
                }

                else{
                    console.log("Don't want to add duplicates");
                }
            }
        }
    };

    return returnFunction;
}
