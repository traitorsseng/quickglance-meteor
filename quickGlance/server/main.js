import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
    Articles.remove({}); // clear the data base
    Locations.remove({}); // clear the geocode data
    var typeOfNews = ["world", "business", "sports", "dining", "technology"];
    console.log("Starting server...");
    for (var i = 0; i < typeOfNews.length; i++){
        console.log("Getting " + typeOfNews[i] + " news...");
        Meteor.call("getNews", typeOfNews[i]); // fetch all the articles of a certain type
    }

    console.log("Testing Bing News Search");
    Meteor.call("searchBingNews", "cricket");

    console.log("Testing Sports Results");
    Meteor.call("getSportScores");
    //Meteor.call("scrapeOther", "http://www.bing.com/cr?IG=93135133AED54EAF912DACEB20BF2AD0&CID=3EB16CAEA085639E15B2651BA19762B1&rd=1&h=BLeyQUo3azrxGpVOkVOQczinQoGF0fvbrlx7VbSwYok&v=1&r=http%3a%2f%2fwww.smh.com.au%2fworld%2fjakarta-governor-ahok-investigated-over-alleged-islam-insult-as-elections-loom-20161018-gs510o.html&p=DevEx,5007.1");

    console.log("Acquired all news events.");
    Meteor.call("storeNewsLocations"); // store their locations
    console.log("Converted all events to the geo-locations(Lat + Lon).");

    console.log("Testing geo scrape.");
    Meteor.call("pyGeoloc", "http://www.abc.net.au/news/2016-10-20/200k-jobs-at-risk-as-housing-boom-rolls-over:-morgan-stanley/7952478");

    console.log("Getting continent data...");
    Meteor.call("storeContinentInfo");
    console.log("Acquired all continent data.");

    console.log("Getting Stock Info...");
    Meteor.call("getStockInfo");

    console.log("Getting ABC news Aus...");
    Meteor.call("getABCNews");

    // Server restarts every day
    var restartFrequency = 1000 * 60 * 60 * 24; // 1 day (1000 millsec * 60 min * 24 hour)
        setTimeout(function(){
            process.exit();
    }, restartFrequency);
});
