// this file will contain client side code for zooming into continents

// local collection of bookmark objects
Bookmarks = new Meteor.Collection(null);

Template.fpanel.onRendered(function () {
    Session.set('numNotifs', "");
    Session.set('numBookmarks', "");
});

Template.fpanel.events({
    // user wants to open a bookmark link to read the article
    'click .bookmarkLink': function(event){
        console.log("User wants to see their bookmarks.");
        Modal.show("readmore");
        Session.set('article_url', this.article_url);
        Session.set('article_id', this.article_title);
        var url = Session.get("article_url");
        Session.set('keep_loading', true);
        Meteor.call('callPy', url, function(err, results){
            if(err) {
                console.log("Error retrieving scraped article");
            } else {
                Session.set('keep_loading', false);
                Meteor.setTimeout(function(){
                    document.getElementById("news-article").innerHTML = results;
                }, 100);
                console.log(results);
            }
        });
    },

    // user wants to remove a bookmark link
    'click .close-notification-button-bookmark': function(event){
        event.preventDefault();
        var bookmark_id = this._id;
        console.log("Removing ID: " + bookmark_id);
        Bookmarks.remove({_id: bookmark_id});
        event.stopPropagation();
    },

    // user wants to open a notification
    'click .notificationLink': function(event) {
        Modal.show("readmore");
        console.log("Article URL TEST: " + this.article_url);
        console.log("Notification TEST Title: " + this.article_title);
        Session.set('article_url', this.article_url);
        Session.set('article_id', this.article_title);
        var url = Session.get("article_url");
        Session.set('keep_loading', true);
        Meteor.call('callPy', url, function(err, results){
            if(err) {
                console.log("Error retrieving scraped article");
            } else {
                Session.set('keep_loading', false);
                Meteor.setTimeout(function(){
                    document.getElementById("news-article").innerHTML = results;
                }, 100);
                console.log(results);
            }
        });
    },

    // user wants to remove a notification
    'click .close-notification-button-notif': function(event){
        event.preventDefault();
        var notification_id = this._id;
        console.log("Removing notification for ID: " + notification_id);
        Notifications.update({_id: notification_id}, {$set: {read_notif : true}});
        // allows the {{each}} to work properly when you remove things during its iteration
        event.stopPropagation();
    },

    // user selected a continent they want to zoom into
    'change .continent': function(event){
        var isChecked = $(event.target).is(":checked");
        var continent = "";
        if (isChecked){
            console.log("User checked a continent:" + $(event.target).val());
            continent = $(event.target).val();
            Session.set('locationZoom', continent);
            setContinentZoom(continent);
            displayForContinent();
        }
    }
});

Template.fpanel.helpers({
    'userBookmarks': function(){
        // return the bookmarks database
        console.log("Updating dropdown");
        console.log("Bookmarks: " + Bookmarks.find().fetch());
        return Bookmarks.find().fetch();
    },

    'chatNotification' : function() {
        return Notifications.find({user_id: Meteor.userId(), read_notif: false}, {
            sort: {timestamp: 1},
            limit: 10
        });
    },

    'numChatNotifications' : function(){
        var notifs = Notifications.find({user_id: Meteor.userId(), read_notif: false}, {
            sort: {timestamp: 1},
            limit: 10
        }).fetch();
        console.log("NUM OF NOTIFICATIONS: " + notifs.length);
        if(notifs.length > 0) {
            Session.set('numNotifs', notifs.length);
        } else if (notifs.length == 0) {
            Session.set('numNotifs', "");
        }
        return Session.get('numNotifs');
    },

    'checkNotifications' : function() {
        var notifs = Notifications.find({user_id: Meteor.userId(), read_notif: false}, {
            sort: {timestamp: 1},
            limit: 10
        }).fetch();
        if(notifs.length == 0) {
            return true;
        } else {
            return false;
        }
    },
    'checkBookmarks' : function() {
        var bm = Bookmarks.find().fetch();
        if(bm.length == 0) {
            return true;
        } else {
            return false;
        }
    },

    'numBookmarks': function(){
        // Meteor.call("getBookmarks", Meteor.userId(), getBookmarksCallback());
        var bm = Bookmarks.find().fetch();
        console.log("NUM BOOKMARKS: " + bm.length);
        if(bm.length > 0) {
            Session.set('numBookmarks', bm.length);
        } else if(bm.length == 0) {
            Session.set('numBookmarks', "");
        }
        return Session.get('numBookmarks');
    }
});
