if(Meteor.isClient) {
    Template.voiceSearch.events ({
        'click .speechRec': function(event) {
            console.log("Pressed Speech Recognition Button");

            var html5api = new Html5Api();
            var speech = html5api.speechRecognition();

            if(speech) {
                Session.set("typeOfNews", "");
                document.getElementById('speechButton').innerHTML = "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>";
                document.getElementById("searchResultsBar").style.width = "300px";
                document.getElementById("navbarID").style.marginLeft = "0px";
                document.getElementById("mySidenav").style.width = "0px";
                // document.getElementById("main").style.marginLeft = "-250px";
                speech.start(function (err, res) {
                    if (err) console.log(err);
                    else {
                        document.getElementById("searchTerm").innerHTML = res.text;
                        console.log(res);
                        //document.getElementById('speechButton').innerHTML = res.text;
                        Session.set("speechResult", res.text);
                        Session.set("display_results", false);
                        Meteor.setTimeout(function () {
                            speech.stop();
                            document.getElementById('speechButton').innerHTML = "<i class='fa fa-microphone' aria-hidden='true'></i>";
                            Session.set("typeOfNews", Session.get("speechResult"));
                            //Modal.show("searchModal");
                            Meteor.setTimeout(function() {
                                Session.set("display_results", true);
                            }, 3000);
                        }, 3000);
                    }
                });
            }
            Meteor.setTimeout(function () {
                console.log("Final recording: " + Session.get("speechResult"));

                console.log("Finished Speech recording");
            }, 6000);
        },

        'keyup input.searchBar': function(event, template) {
            if (event.which === 13) {
                var searchTerm = template.find(".searchBar").value;
                console.log("Search Bar Enter Pressed " + searchTerm);
                                 Meteor.setTimeout(function() {
                                                   Session.set("display_results", true);
                                                   }, 3000);
                Session.set("typeOfNews", searchTerm);
                document.getElementById("searchResultsBar").style.width = "300px";
                document.getElementById("navbarID").style.marginLeft = "0px";
                document.getElementById("mySidenav").style.width = "0px";

                // document.getElementById("main").style.marginLeft = "-250px";
                //Session.set("searchQuery", "justATestVar");
            }
        },
        'click .srchbtn': function(event, template) {
               var searchTerm = template.find(".searchBar").value;
                console.log("Search Bar Enter Pressed " + searchTerm);
                                 Meteor.setTimeout(function() {
                                                   Session.set("display_results", true);
                                                   }, 3000);
                Session.set("typeOfNews", searchTerm);
                document.getElementById("searchResultsBar").style.width = "300px";
                document.getElementById("navbarID").style.marginLeft = "0px";
                document.getElementById("mySidenav").style.width = "0px";
        }

    });

    Template.searchResults.events({
        'click .closeSearchBar': function(e) {
            e.preventDefault();
            console.log("Button Pressed");
            document.getElementById("searchResultsBar").style.width = "0";
            document.getElementById("navbarID").style.marginLeft = "120px";
        },

        'click .read-button': function(event) {
            Modal.show("readmore");
            Session.set('article_url', this.web_url);
            Session.set('article_id', this.headline.main);
            var url = Session.get("article_url");
            Session.set('keep_loading', true);
            Meteor.call('callPy', url, function(err, results){
                if(err) {
                    console.log("Error retrieving scraped article");
                } else {
                    console.log(results);
                    Session.set('keep_loading', false);
                    console.log("Setting the scrape's false first");

                    // wait a 100ms to allow the loading screen to be destroyed
                    Meteor.setTimeout(function(){
                        document.getElementById("news-article").innerHTML = results;
                    }, 100);
                }
            });
        }

    });
}
