if(Meteor.isServer) {
    Meteor.methods ({
        searchBingNews : function(query) {
            var url = "https://api.cognitive.microsoft.com/bing/v5.0/news/search?q=";
            url += query + "&count=10&offset=0&mkt=en-au&safeSearch=Moderate";
            var result = HTTP.call('GET', url,{
                headers: {
                    "Ocp-Apim-Subscription-Key" : "886f041e9bb44d0fa86f44d5984a257a",
                }
            });
            var results = JSON.parse(result.content);
            console.log("Searched News: ");
            return results;
        },
    });
}
