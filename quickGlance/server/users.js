Users = new Mongo.Collection('useraccounts');
SuggestedArticles = new Mongo.Collection('suggestedarticles');

Meteor.methods({
    // adds a new user into the system
    newUser: function(facebook_user_id, newUserData){
        console.log("[New User] Adding new user's preferences: " + facebook_user_id);
        Users.insert(newUserData);
    },

    // retrieves all the locations that user wants to zoom in
    getLocationsToZoom: function(facebook_user_id){
        console.log("Retrieving countries that user wants to zoom in.");
        var usrObject = Users.find({userID: facebook_user_id}).fetch();
        var locationToZoomInto = "";
        if (typeof usrObject != "undefined" && usrObject != ""){
            if (usrObject.length > 0){
                locationToZoomInto = usrObject[0].locationToZoom;
                console.log("Facebook user id: " + facebook_user_id);
                console.log("User's object: " + usrObject[0]);
                console.log("Locations to zoom into: " + usrObject[0].locationToZoom);
            }
        }
        return locationToZoomInto;
    },

    // updates the locations that client wants to zoom in
    updateLocationsToZoom: function(facebook_user_id, locationToZoomIn){
        console.log("Updating all the locations to zoon in on...");
        var usrObject = Users.find({userID: facebook_user_id}).fetch();
        console.log("New location: " + locationToZoomIn);

        if (typeof usrObject != "undefined" && usrObject != ""){
            if (usrObject.length > 0){
                var uniqueDocumentId = usrObject[0]._id;
                Users.update({_id: uniqueDocumentId}, { $set: {locationToZoom: locationToZoomIn} });
            }
        }
    },

    // updates the user's preferences
    updatePreferences: function(facebook_user_id, newPreferences){
        console.log("Updating user's preferences for " + facebook_user_id + ":");
        var usrDat = Users.find({userID: facebook_user_id}).fetch();

        // so the document for a user does exist
        if (typeof usrDat != "undefined" && usrDat != ""){
            if (usrDat.length > 0 && typeof usrDat[0] != "undefined" && usrDat[0] != ""){
                var uniqueDocumentId = usrDat[0]._id;
                console.log("_id = " + usrDat[0]._id);
                // need $set otherwise all the other fields go missing
                Users.update({ _id: uniqueDocumentId}, { $set: {preferences: newPreferences} });
            }
        }
    },

    // retrieves bookmarks for a particular user
    // facebook_user_id = unique ID (string)
    getBookmarks: function(facebook_user_id){
        console.log("User: " + facebook_user_id + " is trying to retrieve their bookmarks");
        var usrObject = Users.find({userID: facebook_user_id}).fetch();
        var bookmarks = [];
        if (typeof usrObject != "undefined" && usrObject != ""){
            if (usrObject.length > 0 && typeof usrObject[0] != "undefined" && usrObject[0] != ""){
                console.log("Returned bookmarks");
                bookmarks = usrObject[0].bookmarkedArticles;
            }
        }

        return bookmarks;
    },

    // updates the bookmarks
    // newBookmarks = a bookmark object
    // where each bookmark object contains a url as well as the title of the article
    updateBookmarks: function(facebook_user_id, newBookmarks){
        var usrObject = Users.find({userID: facebook_user_id}).fetch();
        console.log("\nUpdating bookmarks for user: ");
        console.log(usrObject);
        console.log("New bookmarks: ");
        console.log(newBookmarks + "\n");
        // var existingBookmarks = usrObject[0].bookmarkedArticles;
        // existingBookmarks.push(newBookmarks);
        if (typeof usrObject != "undefined" && usrObject != ""){
            if (usrObject.length > 0 && typeof usrObject[0] != "undefined" && usrObject[0] != ""){
                var uniqueDocumentId = usrObject[0]._id;
                Users.update({_id: uniqueDocumentId}, { $set: {bookmarkedArticles: newBookmarks} });
            }
        }
    },

    // retrieves the preferences for a specific user
    getPreferences: function(facebook_user_id){
        console.log("Trying to retrieve your preferences...");
        console.log("UserId = " + facebook_user_id);
        console.log("These are your preferences: " + Users.find({userID: facebook_user_id}).fetch());
        var userObject = Users.find({userID: facebook_user_id}).fetch();
        var pref = [];
        if (typeof userObject != "undefined" && userObject != ""){
            if (userObject.length > 0 && typeof userObject[0] != "undefined" && userObject[0] != ""){
                pref = userObject[0].preferences;
            }
        }

        return pref;
    },

    //Add to subscribed articles
    addToSubscribedArticles: function(user_id, article_name) {
        console.log("Adding to subscribedArticles");
        Users.update({userID: user_id}, { $push: {subscribedArticles: article_name} });
        console.log(Users.find({userID: user_id}).fetch())
    },

    //check subscribed articles
    checkSubscribedArticles: function (user_id, article_id) {
        console.log("Getting subscribedArticles");
        var returnValue = Users.find({userID: user_id}).fetch();
        var exists = false;
        if(returnValue.length > 0) {
            var articlesArr = returnValue[0].subscribedArticles;
            for(var i = 0; i < articlesArr.length; i++) {
                console.log("ArticlesArr: " + articlesArr[i] + " , article Id: " + article_id);
                if(articlesArr[i] == article_id) {
                    exists = true;
                }
            }
        }
        console.log("checkSubscribedArticles status: " + exists);
        return exists;
    },

    // Retrieves boolean to check if first time login
    getFirstLoginStatus: function(facebook_user_id){
        console.log("Retieving login status for " + facebook_user_id);
        var returnValue = Users.find({userID: facebook_user_id}).fetch();
        if (typeof returnValue != "undefined" && returnValue.length > 0){
            return returnValue[0].firstTimeLogin;
        }
        else{
            return true;
        }
    },

    //Changing login Status to false
    changeLoginStatus: function(user_id){
        console.log("Changing login status for " + user_id);
        var returnValue = Users.find({userID: user_id}).fetch();
        Users.update({userID: user_id}, { $set: {firstTimeLogin: false} });
        console.log(Users.find({userID: user_id}).fetch());
    },

    // clears the preferences for an existing user
    clearPreferences: function(facebook_user_id){
        var usrDat = Users.find({userID: facebook_user_id}).fetch();
        if (typeof usrDat[0] != "undefined"){
            usrDocumentId = usrDat[0]._id;
            Users.remove({ _id: usrDocumentId });
        }
        else{
            console.log("Couldn't delete non-existant user");
        }
    },

    // checks to see if a user exists on the system
    doesUserExist: function(facebook_user_id){
        var existBool = true;
        console.log("Checking if user exists...");
        if (Users.find({userID: facebook_user_id}).fetch() == "" || typeof Users.find({userID: facebook_user_id}).fetch() == "undefined"){
            existBool = false;
        }
        console.log("User exists: " + existBool);
        return existBool;
    },

    //  Add to keywords
    addToKeywords: function(user_id, keywords) {
        console.log("Adding to Keywords");
        var user = Users.find({userID: user_id}).fetch();
        var topics = user[0].interestedTopics;
        var firstTopic = topics[0];
        if(topics[topics.length-1] != keywords && keywords.length > 0) {
            if(topics.length == 5) {
                Users.update({userID: user_id}, { $pull: {interestedTopics: firstTopic} });
                Users.update({userID: user_id}, { $push: {interestedTopics: keywords} });
                console.log("Popped firt and added");
            } else {
                Users.update({userID: user_id}, { $push: {interestedTopics: keywords} });
                console.log("Added to Keywords!!");
            }
        } else {
            console.log("Already in topic list");
        }
    },

    checkKeywords: function(user_id) {
        console.log("Getting users preffered topics");
        var returnValue = Users.find({userID: user_id}).fetch();
        if(returnValue.length > 0) {
            var articlesArr = returnValue[0].interestedTopics;
        }
        console.log("Interested Topics " + articlesArr);
        return articlesArr;
    },

    findArticlesWithKeywords: function(user_id) {
        console.log("Getting users articles based on intelligent assistant");
        var returnValue = Users.find({userID: user_id}).fetch();
        var topics = [];
        var articles = [];
        if(returnValue.length > 0) {
            topics = returnValue[0].interestedTopics;
            SuggestedArticles.remove({user: user_id});
            var articlesToAdd = "";
            var arrayOfArticles = [];
            var artsToAdd = {
                user: user_id,
                articles: arrayOfArticles
            };
            SuggestedArticles.insert(artsToAdd);
            for(var i = 0; i < topics.length; i++) {
                console.log("Finding Articles based on : " + topics[i]);
                articlesToAdd = Meteor.call("searchBingNews", topics[i]);
                // for(var j = 0; j < articlesToAdd.length; j++) {
                //     SuggestedArticles.insert(articlesToAdd[j]);
                // }
                // var artsToAdd = {
                //     user: user_id,
                //     articles: articlesToAdd
                // };

                SuggestedArticles.update({user: user_id}, { $push: {articles: articlesToAdd} });
                articles.push(articlesToAdd);
            }
        }
        console.log("Interested Topics " + articles);
        return articles;
    }
});
