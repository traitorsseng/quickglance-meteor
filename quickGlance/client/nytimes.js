SuggestedArticles = new Mongo.Collection('suggestedarticles');

if(Meteor.isClient) {
    // do nothing
    // Meteor.call("getNews");
    // Some JS files get loaded before others especially if they're in the same folder
    Template.sideBar.helpers ({
        articles(type) {
            return Articles.find({typeNews: type}).fetch();
        },

        url(){
            return Session.get('article_url');
        },

        title(){
            return Session.get('article_id');
        },

        image(){
            return Session.get('article_img');
        },
    });

    Template.recommendations.helpers ({
        recArticles(){
            console.log("Finding Articles");
            console.log(Articles.findOne({typeNews: "world"}));
            var ar = Articles.findOne({typeNews: "world"});
            var retArray = ar.articles;
            var array = [];
            if(retArray.length > 38) {
                for(var i = 30; i < 38; i++) {
                    array.push(retArray[i]);
                }
            } else {
                for(var i = 0; i < 8; i++) {
                    array.push(retArray[i]);
                }
            }

            return array;
        },
        suggestedArticles(){
            console.log("Retrieving recommended articles...");
            var array = [];
            var user = SuggestedArticles.findOne({user: Meteor.userId()});
            if(typeof user != "undefined") {
                var articles = user.articles;
                console.log(articles);
                var val = "";
                var article = "";
                for(var i = 0; i < articles.length; i++) {
                    val = articles[i].value;
                    var x = Math.floor((Math.random() * val.length) );
                    var y = Math.floor((Math.random() * val.length) );
                    if(x == y && y != val.length) {
                        x++;
                    }
                    for(var j = 0; j < val.length; j++) {
                        article = val[j];
                        console.log(article);
                        if(articles.length == 1) {
                            array.push(article);
                        } else if (articles.length == 2) {
                            if(j < 5) {
                                array.push(article);
                            }
                        } else if (articles.length == 3) {
                            if(j < 4) {
                                array.push(article);
                            }
                        } else if (articles.length == 4) {
                            if(j < 3) {
                                array.push(article);
                            }
                        } else {
                            if(j == x || j == y){
                                array.push(article);
                            }
                        }
                    }
                }
            }
            array = shuffle(array);
            console.log(array);
            return array;
        }
    });

    Template.searchResults.helpers ({
        'checkResults': function(){
            return Session.get("display_results");
        },

        'articles' : function() {
            console.log("Type of News: " + Session.get('typeOfNews'));
            var searchTerm = Session.get('typeOfNews');
            var array = [];
            var searchArray = [];
            var allNewsObjects = Articles.find({}).fetch();
            if(searchTerm != undefined) {
                searchTerm = searchTerm.toLowerCase();
                console.log("Search Term: " + searchTerm);

                //Regex unncessary words
                searchTerm = searchTerm.replace(/\bshow\b/g,'');
                searchTerm = searchTerm.replace(/\bnews\b/g,'');
                searchTerm = searchTerm.replace(/\babout\b/g,'');
                searchTerm = searchTerm.replace(/\bhappening\b/g,'');
                searchTerm = searchTerm.replace(/\bgoing\b/g,'');
                searchTerm = searchTerm.replace(/\bme\b/g,'');
                searchTerm = searchTerm.replace(/\bin\b/g,'');
                searchTerm = searchTerm.replace(/\bwhat\b/g,'');
                searchTerm = searchTerm.replace(/\bis\b/g,'');
                searchTerm = searchTerm.replace(/\bon\b/g,'');
                searchTerm = searchTerm.replace(/\bthe\b/g,'');
                searchTerm = searchTerm.replace(/ +/g, ' ');
                searchTerm = searchTerm.replace(/^\s+|\s+$/g,'');

                console.log("Regexed string: " + searchTerm);

                var result = ReactiveMethod.call("searchNews", searchTerm);
                if(result != undefined) {
                    console.log("Reactive method call result: " + result.response.docs.length);
                    for(var i = 0; i < result.response.docs.length; i++) {
                        var currArt = result.response.docs[i];
                        searchArray.push(currArt);
                    }
                }
                console.log("SearchArray Length: " + searchArray.length);


                // Meteor.call("searchNews", searchTerm, function(err, results) {
                //     if(err) {
                //         console.log("Error searching news");
                //     } else {
                //         console.log(results);
                //     }
                // });
                //Split search term to key words
                var terms = searchTerm.split(" ");
                for(var i = 0; i < allNewsObjects.length; i++) {
                    var currentNewsObject = allNewsObjects[i];
                    for(var j = 0; j < currentNewsObject.articles.length; j++) {
                        var currentArticle = currentNewsObject.articles[j];
                        var title = (currentArticle.title).toLowerCase();
                        var abstract = (currentArticle.abstract).toLowerCase();
                        var section = (currentArticle.section).toLowerCase();
                        if(title.includes(searchTerm) || abstract.includes(searchTerm)
                            || section.includes(searchTerm) ) {
                                array.push(currentArticle);
                            }
                    }
                }
            }

//            for(var i = 0; i < searchArray.length; i++) {
//                console.log("Array of Searched Articles: " + searchArray[i].headline.main);
//            }

            var ar = Articles.find({typeNews: Session.get('typeOfNews')}).fetch();
            console.log("Articles: " + ar);
            // if(array.length > 0) {
            //     //addMarkers(array);
            //     return array;
            // } else if(searchTerm != undefined){
            //     var empty = { title: 'No Results Found', abstract: ''};
            //     array.push(empty);
            //     return array;
            // } else {
            //     var empty = { title: 'Searching....', abstract: ''};
            //     array.push(empty);
            //     return array;
            // }
            console.log("Display results: " + Session.get("display_results"));
            if (Session.get("display_results") == true){
                if(searchArray.length > 0) {
                    console.log(searchArray);
                    console.log("Results Found");
                    //searchArray.pop();
                    //Session.set("display_results", false);
                    //addMarkers(array);
                    if(Meteor.userId()){
                        console.log("Searched For Results");
                        console.log(Session.get("typeOfNews"));
                        Meteor.call("addToKeywords", Meteor.userId(), searchTerm);
                        Meteor.call("findArticlesWithKeywords", Meteor.userId(), function(err, results) {
                            if(err) {
                                console.log("Error getting keywords");
                            } else {
                                console.log("Results for suggested articles: ");
                                console.log(results);
                            }
                        });
                    }
                    return searchArray;
                } else {
                    Session.set("display_results", false);
                    console.log("No results found");
                    var empty = { headline : {main: 'No Results Found'} , lead_paragraph: ''};
                    searchArray.push(empty);
                    return searchArray;
                }
            }

            else{
                var emptyArray = [];
                var empty = { headline : {main: 'Searching...'} , lead_paragraph: ''};
                emptyArray.push(empty);
                return emptyArray;
            }
        },
        'typeOfNews' : function() {
            return Session.get('typeOfNews');
        }
    });

    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
}
