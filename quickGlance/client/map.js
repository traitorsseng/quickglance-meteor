Locations = new Mongo.Collection("locations");
Articles = new Mongo.Collection("articles"); // this allows us to have a copy of the server's database (just for now)

// http://wiki.openstreetmap.org/wiki/Nominatim (Alternative geocoding service)

//if (Meteor.isClient) {
Meteor.startup(function() {
    GoogleMaps.load({key: "AIzaSyCV6I1ZJ97zbkm2CgnbU-WVPG2C6aiD4B0"});
});

Template.body.helpers({
    exampleMapOptions: function() {
        // Make sure the maps API has loaded
        if (GoogleMaps.loaded()) {
            var style = [];
            //  Change map based on time of day
            //  Morning
            if(moment().format("HH") <  12 && moment().format("HH") >= 6) {
                style = [{"featureType":"all","elementType":"geometry","stylers":[{"color":"#034862"}]},
                       {"featureType":"all","elementType":"geometry.fill","stylers":[{"visibility":"simplified"},
                       {"color":"#034862"}]},{"featureType":"all","elementType":"geometry.stroke","stylers":
                       [{"visibility":"simplified"},{"color":"#034862"}]},{"featureType":"all","elementType":"labels","stylers":[{"color":"#ffffff"}]},{"featureType"
                       :"all","elementType":"labels.text.fill","stylers":[{"gamma":0.01},
                       {"lightness":20},{"color":"#ffffff"}]},{"featureType":"all","elementType"
                       :"labels.text.stroke","stylers":[{"saturation":-31},{"lightness":-33},{"weight":2},
                       {"gamma":0.8},{"color":"#034862"}]},{"featureType":"all","elementType":"labels.icon"
                       ,"stylers":[{"visibility":"off"},{"color":"#034862"}]},{"featureType":"administrative"
                       ,"elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType"
                       :"landscape","elementType":"geometry","stylers":[{"lightness":30},{"saturation":30}]},
                       {"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#046e95"}]},
                       {"featureType":"poi","elementType":"geometry","stylers":[{"saturation":20}]},{"featureType"
                       :"poi.park","elementType":"geometry","stylers":[{"lightness":20},{"saturation":-20}]},
                       {"featureType":"road","elementType":"geometry","stylers":[{"lightness":10},
                       {"saturation":-30}]},{"featureType":"road","elementType": "geometry.stroke"
                       ,"stylers":[{"saturation":25},{"lightness":25}]},
                       {"featureType": "water","elementType":"all","stylers":[{"lightness":-20}]}];
            }
            //  Afternoon
            else if (moment().format("HH") >= 12 && moment().format("HH") <= 19){
                 style = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]}, {"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#2c5a71"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]}];
            }
            //  Night
            else {
                 style = [{"featureType":"all","elementType":"geometry","stylers":[{"color":"#1d2c4d"}]},
                    {"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#8ec3b9"}]},
                    {"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#1a3646"}]},
                    {"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[
                    {"color":"#4b6878"}]},{"featureType":"administrative.province","elementType"
                    :"geometry.stroke", "stylers":[{"color":"#4b6878"}]},{"featureType":"administrative.neighborhood"
                    ,"elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]
                    },{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[
                    {"color":"#64779e"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke"
                    ,"stylers":[{"color":"#334e87"}]},{"featureType":"landscape.natural","elementType":"geometry"
                    ,"stylers":[{"color":"#023e58"}]},{"featureType":"poi","elementType":"geometry","stylers":[
                    {"color":"#283d6a"}]},{"featureType":"poi","elementType":"labels.text","stylers":[
                    {"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[
                    {"color":"#6f9ba5"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[
                    {"color":"#1d2c4d"}]},{"featureType":"poi.business","elementType":"all","stylers":[
                    {"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[
                    {"color":"#023e58"}]},{"featureType":"poi.park","elementType":"labels.text","stylers":[
                    {"visibility":"off"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[
                    {"color":"#3C7680"}]},{"featureType":"road","elementType":"geometry","stylers":[
                    {"color":"#304a7d"}]},{"featureType":"road","elementType":"labels","stylers":[
                    {"visibility":"off"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[
                    {"color":"#98a5be"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[
                    {"color": "#1d2c4d"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[
                    {"color": "#2c6675"}]},{"featureType":"road.highway","elementType":"geometry.stroke"
                    ,"stylers":[{"color":"#255763"}]},{"featureType":"road.highway","elementType":"labels.text.fill"
                    ,"stylers":[{"color":"#b0d5ce"}]},{"featureType":"road.highway","elementType"
                    :"labels.text.stroke","stylers":[{"color":"#023e58"}]},{"featureType":"transit"
                    ,"elementType":"labels.text.fill","stylers":[{"color":"#98a5be"}]},
                    {"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},
                    {"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"color":"#283d6a"}]},
                    {"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#3a4762"}]},
                    {"featureType":"water","elementType":"geometry","stylers":[{"color":"#0e1626"}]},
                    {"featureType":"water","elementType":"labels.text","stylers":[{"visibility":"off"}]},
                   {"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#4e6d70"}]}];
            }
            // Map initialization options
            return {
                center: new google.maps.LatLng(0,0),
                zoom: 3,
                streetViewControl: false,
                mapTypeControl: false,
                //styles: [{"stylers":[{"hue":"#ff1a00"},{"invert_lightness":true},{"saturation":-100},{"lightness":33},{"gamma":0.5}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2D333C"}]}]
                styles: style
                //styles: [{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#e3e3e3"}]},{"featureType":"landscape.natural","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"color":"#cccccc"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"transit.station.airport","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#FFFFFF"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]}]
            }
        }
    }
});

Template.body.onCreated(function(){
    // We can use the `ready` callback to interact with the map API once the map is ready.

    GoogleMaps.ready('map', function(map){
        // create the hash table
        var markers = []; // hash table, where key is the type of news and the value is the array of articles of the same news type
        var continentsData = [];
        var allTypesOfNews = ['world', 'business', 'sports', 'dining', 'technology'];
        var allContinents = ['Africa', 'South America', 'North America', 'Oceania', 'Europe', 'Asia', 'Antarctica'];
        var countryCodesCon = loadCountryCodes();
        var newsInContinents = []; // hash table, where each key is a continent and the value is the array of articles in that continent
        var continentToDisplay = null; // global variable
        var markersMapCluster = [];
        var markersToAdd = [];

        // initialise the hash tables
        for (var i = 0; i < allTypesOfNews.length; i++){
            var emptyArray = [];
            markers[allTypesOfNews[i]] = emptyArray;
        }

        for (var i = 0; i < allContinents.length; i++){
            newsInContinents[allContinents[i]] = [];
        }

        //  Initialising clusters
        var mcOptions = {gridSize: 40, maxZoom: 8, zoomOnClick: false, imagePath: 'https://raw.githubusercontent.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m'};
        var mc = new MarkerClusterer(GoogleMaps.maps.map.instance, markersMapCluster, mcOptions);
        var iw = new google.maps.InfoWindow();

        createMapWithMarkers();

        // some global functions ????
        createMap = createMapWithMarkers;
        addMarkers = addMarkersMap;
        removeMarkers = removeMarkersMap;
        getContinentData = getContinentLatLon;
        centerContinent = setCenterMap;
        setZoomQuickGlance = setMapZoom;
        displayForContinent = displayMarkersContinent;
        setContinentZoom = setContinent;
        // showArticle = clusterArticle;
        removeAllMarkers = removeAllMarkersMap;

        function createMapWithMarkers() {
            console.log("Creating Map with Markers");
            // removeMarkers();
            Meteor.call("getPreferences", Meteor.userId(), function(err, results){
                if (err){
                    console.log("Error retrieving the user's preferences.\n");
                }

                else{
                    // var newsWeWant = results[0].preferences;
                    var newsWeWant;
                    // console.log(results.length);
                    // user's preferences haven't been stored so use local storage
                    console.log("Results: " + results);
                    if (typeof results == "undefined" || results == "" || results.length == 0){
                        console.log("Retrieving from local storage...");
                        //newsEvents = newsWeWant; // replace this will a method equivalent of fetching from local storage
                        newsWeWant = [];
                        // Meteor.call("newUser", Meteor.userId(), newsWeWant);
                    }

                    else{
                        console.log("Retrieved from server...\n");
                        newsWeWant = results;
                    }
                    console.log(newsWeWant.length);
                    // convert each news article into latitude/longitude information
                    for (var i = 0; i < newsWeWant.length; i++){
                        //console.log("in loop");
                        Meteor.call("getNewsLocations", newsWeWant[i], GNLcallback(newsWeWant[i], i));
                    }
                }
            });

            // fetches the lat and lon for each continent
            Meteor.call("getContinentsInfo", function(err, results){
                if (err){
                    console.log("Error acquiring continent info :X");
                }

                else{
                    console.log("Getting continent info");
                    fillContinentData(results);
                    console.log("Setting center at: " + Session.get('locationZoom'));
                    setContinent(Session.get('locationZoom'));
                    displayMarkersContinent();
                    JSON.stringify("Results2: " + results);
                }
            });
        }

        // sets the continent defiend flag on so that global news isnt shown when a continent is selected
        function setContinent(continent){
            continentToDisplay = continent;
        }

        // stores data aobut continents into a local array
        function fillContinentData(data){
            for (var i = 0; i < data.length; i++){
                continentsData.push(data[i]);
            }

            console.log("local continentsData: " + continentsData);
        }

        // stores the articles based on continents
        function filterContinents(){
            for (var continent in newsInContinents){
                newsInContinents[continent] = [];
            }

            for (var type in markers){
                // console.log("Type = " + type);
                var markerArray = markers[type];
                for (var i = 0; i < markerArray.length; i++){
                    var currentMarker = markerArray[i];
                    var cc = currentMarker.countryCode;
                    // console.log("Country code = " + cc + " CC conversion = " + countryCodesCon[cc]);
                    if (cc == null){
                        continue;
                    }
                    newsInContinents[countryCodesCon[cc]].push(currentMarker);
                }
            }
        }

        // displays all the markers in a continent and zooms into that continent as well
        function displayMarkersContinent(){
            if (continentToDisplay == "World"){
                // restore all the markers
                setContinent(null);
                showAllMarkers();
                setCenterMap(0,0);
                setMapZoom(3);
            }

            else{
                var markersToHide = [];
                console.log("Displaying markers for: " + continentToDisplay);
                for (var curContinent in newsInContinents){
                    var markerArray = newsInContinents[curContinent];
                    if (curContinent == continentToDisplay){
                        // show all the markers in the array
                        for (var i = 0; i < markerArray.length; i++){
                            markerArray[i].marker.setMap(map.instance);
                        }
                    }

                    else{
                        console.log(markerArray);

                        // hide all the markers in the array
                        for (var i = 0; i < markerArray.length; i++){
                            markerArray[i].marker.setMap(null);
                            markersToHide.push(markerArray[i].marker);
                        }

                    }
                }
                mc.removeMarkers(markersToHide);
                mc.resetViewport();
                mc.redraw();
                // change the center of the map
                var continentData = getContinentLatLon();
                setCenterMap(continentData.lat, continentData.lon);
                setMapZoom(4);
            }
        }

        // retrives the lat and lon for given continents
        function getContinentLatLon(){
            var continentObject = "";
            for (var i = 0; i < continentsData.length; i++){
                if (continentsData[i].name == continentToDisplay){
                    continentObject = continentsData[i];
                    break;
                }
            }
            return continentObject;
        }

        function addMarkersMap(articlesToAdd){
            for (var i = 0; i < articlesToAdd.length; i++){
                Meteor.call("getNewsLocations", articlesToAdd[i], GNLcallback(articlesToAdd[i], i));
            }
        }

        function removeMarkersMap(articlesToRemove){
            console.log(articlesToRemove);
            for (var i = 0; i < articlesToRemove.length; i++){
                var type = articlesToRemove[i];
                var markerArray = markers[type];
                console.log(markerArray);
                for (var j = 0; j < markerArray.length; j++){
                    markerArray[j].marker.setMap(null);
                }
                console.log("Map Cluster Markers Removing.. " + markersMapCluster.length);
                console.log(markersMapCluster);
                var k = 0;
                var markersToRemove = [];
                while(k < markersMapCluster.length) {
                    if(markersMapCluster[k].typeOfNews == type) {
                        console.log("Removing markers from cluster : " + k);
                        //markersMapCluster.splice(k, 1);
                        markersToRemove.push(markersMapCluster[k]);
                        console.log("Size of array: " + markersMapCluster.length);
                    } //else {
                        k++;
                    //}
                }
                console.log("AFTER Map Cluster Markers Removing");
                console.log(markersMapCluster);
                //mc.setMap(null);
                //mc.clearMarkers();


                mc.removeMarkers(markersToRemove);
                mc.resetViewport();
                mc.redraw();
                console.log("Getting Markers After: ");
                console.log(mc.getMarkers());
                // markersMapCluster = [];
                // clear all the articles
                markers[articlesToRemove[i]] = [];
                //mc = new MarkerClusterer(GoogleMaps.maps.map.instance, markersMapCluster, mcOptions);
                console.log("Markers Array AFTER: ");
                console.log(markers['world']);
            }

            filterContinents();
            console.log("After removing some news of a certian type: \n" + markers);
        }

        // shows all the markers stored in the array of markers
        function showAllMarkers(){
            var allMarkers = [];
            for (var type in markers){
                var markerArray = markers[type];
                for (var i = 0; i < markerArray.length; i++){
                    markerArray[i].marker.setMap(map.instance);
                    allMarkers.push(markerArray[i].marker);
                }
            }
            mc.addMarkers(allMarkers);
            mc.resetViewport();
            mc.redraw();
        }

        // hides all the markers in the array of markers
        function hideAllMarkers(){
            var allMarkers = [];
            for (var type in markers){
                var markerArray = markers[type];
                for (var i = 0; i < markerArray.length; i++){
                    markerArray[i].marker.setMap(null);
                    allMarkers.push(markerArray[i].marker);
                }
            }
            mc.removeMarkers(allMarkers);
            mc.resetViewport();
            mc.redraw();
        }

        function removeAllMarkersMap(){
            removeMarkersMap(allTypesOfNews);
        }

        function GNLcallback(typeOfNews, index){
            // console.log("Callback for " + typeOfNews + " news.\n");
            var result = function(err, results){
                if (err){
                    console.log("Error acquiring latitude and longitude information for: " + typeOfNews + ".\n");
                }

                else{
                    // console.log("Else statement: Callback for " + typeOfNews + " news.\n");
                    locations = results;
                    markersToAdd = [];
                    // console.log("Locations = " + locations);
                    console.log("CREATING NEWS FOR: " + typeOfNews + " length of results: " + locations.length);
                    for (var i = 0; i < locations.length; i++){
                        var location = locations[i]; // location is actually an object
                        var article = location.articleBelongTo;
                        var loc = article.geo_facet[0];
                        var lat = location.lat;
                        var lon = location.lon;
                        var title = article.title;
                        var abstract = article.abstract;
                        var url = article.url;
                        var image = "";
                        var countryCode = location.countryCode;
                        var keywords = "";
                        if(typeof article.des_facet != "undefined" && article.des_facet.length > 0) {
                            keywords = article.des_facet[0];
                        }
                        if (typeof article.multimedia != "undefined" && article.multimedia != ""){
                            // console.log("Index = " + i + "  location: " + loc);
                            image = article.multimedia[1].url;
                        } else if (typeof article.media != "undefined" && article.media != "") {
                            //console.log(article);
                            image = article.media[0]['media-metadata'][1].url;
                        }
                        createMarker(lat, lon, loc, title, abstract, image, i, url, typeOfNews, countryCode, keywords);
                    }
                    console.log("Markers Cluster : " + markersMapCluster.length);
                    console.log("Markers to ADD Array: " + markersToAdd.length);
                    mc.addMarkers(markersToAdd);
                    mc.resetViewport();
                    mc.redraw()
                    //var mcOptions = {gridSize: 50, maxZoom: 15, imagePath: 'https://raw.githubusercontent.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m'};
                    // mc = new MarkerClusterer(GoogleMaps.maps.map.instance, markersMapCluster, mcOptions);
                    // var markerClusters = mc.clusters_;
                    // console.log(markerClusters);
                    // console.log(mc);
                    // console.log(markersMapCluster);
                    google.maps.event.addListener(mc, 'clusterclick', function(cluster) {
                        var m = cluster.getMarkers();
                        //console.log(m);
                        var contentString = "<div class='list-group iw-text'>";
                        for(var i = 0; i < m.length; i++) {
                            var marker = m[i];
                            console.log(m[i]);
                            var buttonID = "markerButton" + i;
                            console.log(buttonID);
                            var abstract = marker.abstract;
                            var title = marker.newsTitle;
                            abstract = abstract.replace(/'/g, "\\'");
                            title = title.replace(/'/g, "\\'");
                            contentString += '<button class="list-group-item" onclick="javascript:clickArticle(\'' + title + '\', \'' + marker.url +
                                '\', \'' + marker.image + '\', \'' + abstract + '\', \'' + marker.keywords + '\');">'
                                 + '<img src="' + marker.icon + '">' + " " + m[i].newsTitle +
                                 "</button><br><br>";
                        }
                        contentString += "</div>";
                        iw.setContent(contentString);
                        iw.setPosition(cluster.getCenter());
                        iw.open(GoogleMaps.maps.map.instance);
                        map.instance.panTo(cluster.getCenter());
                        //console.log(m.length);
                        console.log(cluster.getSize());
                    });
                    filterContinents();
                    // refresh after the client has added more markers
                    if (continentToDisplay != null){
                        displayMarkersContinent();
                    }
                    console.log("Successfully acquired geo-data and created all markers for the data.\n");
                }
            };

            return result;
        }

        /* ================ FUNCTION DEFINITIONS ================== */
        function doSetTimeout(i) {
            setTimeout( function(){
                var addr = nyTimesArticles[i].geo_facet[0];
                //console.log(i);
                geocoder.geocode({address: addr}, geocodeCallBack(i));
            }, i*600);
        }

        window.clickArticle = function(title, url, image, abstract, keywords) {
        	console.log("Clicked Article");
            console.log(title);
            console.log(url);
            //console.log(marker);
            //var abstract = marker.abstract;
            var contentString = '<h4>' + title + '</h4><p>' + abstract + "</p><img class='img-thumbnail' src='" + image + "'></img>";
            document.getElementById('news-summary').innerHTML = contentString;
            Session.set('article_id', title);
            Session.set('article_url', url);
            Session.set('article_img', image);
            Session.set('keywords', keywords);
            //console.log("Index:" + index);
            console.log("Article id:" + title);
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("navbarID").style.marginLeft = "200px";
            document.getElementById("searchResultsBar").style.width = "0px";
            // document.getElementById("main").style.marginLeft = "250px";

        }

        // callback function for geocode: doing it like this allows index to be passed which solves the abvoe problem
        // http://stackoverflow.com/questions/7002973/google-maps-api-v3-pass-more-information-into-geocode-call-back

        // creates a marker and puts it in the map
        function createMarker(lat, lon, loc, title, abstract, image, index, url, type, countryCode, keywords) {
            console.log("Creating markers");
            lat += jitter();
            lon += jitter();
            var latlng = new google.maps.LatLng(lat, lon);
            //  Accessing the map instance
            var mapI = GoogleMaps.maps.map.instance;
            //console.log(mapI)
            abstract = abstract.replace(/\"/g,"'");
            //  Marker HTML Content
            var contentString = '<h4>' + title + '</h4><p>' + abstract + "</p><img class='img-thumbnail' src='" + image + "'></img>";
            var hoverWindow =  '<p><b>' + title + '</b></p>' + "<div class='hover-text'>" + '<p>Click to expand</p></div>';

            //  Infowindow
            var infowindow = new google.maps.InfoWindow({
                content: hoverWindow,
                //disableAutoPan: true,
                maxWidth: 165
            });

            // Types of icons
            var icons = {
                world: {
                    icon : '/images/global.png'
                },
                business : {
                    icon: '/images/graphic.png'
                },
                technology: {
                    icon: '/images/smartphone.png'
                },
                dining: {
                    icon: '/images/cutlery.png'
                },
                sports: {
                    icon: '/images/football-1.png'
                }
            };

            loc = new google.maps.Marker({
                map: mapI,
                position: latlng,
                icon: icons[type].icon
            });

            loc.customInfo = contentString;
            loc.typeOfNews = type;
            loc.newsTitle = title;
            loc.url = url;
            loc.image = image;
            loc.abstract = abstract;
            loc.keywords = keywords;

            // user has not selected a continent to zoom into
            if (continentToDisplay == null){
                loc.setMap(map.instance);
            }

            var markerData = {
                marker: loc,
                countryCode: countryCode,
                newsType: type
            };
            markersToAdd.push(loc);
            markersMapCluster.push(loc);
            //mc.addMarkers(loc);
            markers[type].push(markerData);

            loc.addListener('mouseover', function() {
                infowindow.open(map, loc);
            });

            loc.addListener('mouseout', function() {
                infowindow.close();
            });

            loc.addListener('click', function() {
                document.getElementById('news-summary').innerHTML = contentString;
                Session.set('article_id', title);
                Session.set('article_url', url);
                Session.set('article_img', image);
                Session.set('keywords', keywords);
                console.log("Index:" + index);
                console.log("Article id:" + title);
                document.getElementById("mySidenav").style.width = "300px";
                document.getElementById("navbarID").style.marginLeft = "200px";
                document.getElementById("searchResultsBar").style.width = "0px";
                // document.getElementById("main").style.marginLeft = "250px";
            });
        }

        // wrapper that sets the center of the map
        function setCenterMap(lat, lon){
            var latlng = new google.maps.LatLng(lat, lon);
            console.log("Map = " + map);
            map.instance.panTo(latlng);
            // setMapZoom(4);
        }

        // wrapper that sets the map's zoom level
        function setMapZoom(zoom){
            map.instance.setZoom(zoom);
        }

        //  Function that offsets by random value
        function jitter(){
            var j = Math.random() - .5; // -.5:.5
            return j * 1;
        }

        // returns a hash of all the country codes and their corresponding continent
        function loadCountryCodes(){
            var countryCodes = [];
            countryCodes["AD"] = "Europe";
            countryCodes["AE"] = "Asia";
            countryCodes["AF"] = "Asia";
            countryCodes["AG"] = "North America";
            countryCodes["AI"] = "North America";
            countryCodes["AL"] = "Europe";
            countryCodes["AM"] = "Asia";
            countryCodes["AN"] = "North America";
            countryCodes["AO"] = "Africa";
            countryCodes["AQ"] = "Antarctica";
            countryCodes["AR"] = "South America";
            countryCodes["AS"] = "Oceania";
            countryCodes["AT"] = "Europe";
            countryCodes["AU"] = "Oceania";
            countryCodes["AW"] = "North America";
            countryCodes["AZ"] = "Asia";
            countryCodes["BA"] = "Europe";
            countryCodes["BB"] = "North America";
            countryCodes["BD"] = "Asia";
            countryCodes["BE"] = "Europe";
            countryCodes["BF"] = "Africa";
            countryCodes["BG"] = "Europe";
            countryCodes["BH"] = "Asia";
            countryCodes["BI"] = "Africa";
            countryCodes["BJ"] = "Africa";
            countryCodes["BM"] = "North America";
            countryCodes["BN"] = "Asia";
            countryCodes["BO"] = "South America";
            countryCodes["BR"] = "South America";
            countryCodes["BS"] = "North America";
            countryCodes["BT"] = "Asia";
            countryCodes["BW"] = "Africa";
            countryCodes["BY"] = "Europe";
            countryCodes["BZ"] = "North America";
            countryCodes["CA"] = "North America";
            countryCodes["CC"] = "Asia";
            countryCodes["CD"] = "Africa";
            countryCodes["CF"] = "Africa";
            countryCodes["CG"] = "Africa";
            countryCodes["CH"] = "Europe";
            countryCodes["CI"] = "Africa";
            countryCodes["CK"] = "Oceania";
            countryCodes["CL"] = "South America";
            countryCodes["CM"] = "Africa";
            countryCodes["CN"] = "Asia";
            countryCodes["CO"] = "South America";
            countryCodes["CR"] = "North America";
            countryCodes["CU"] = "North America";
            countryCodes["CV"] = "Africa";
            countryCodes["CX"] = "Asia";
            countryCodes["CY"] = "Asia";
            countryCodes["CZ"] = "Europe";
            countryCodes["DE"] = "Europe";
            countryCodes["DJ"] = "Africa";
            countryCodes["DK"] = "Europe";
            countryCodes["DM"] = "North America";
            countryCodes["DO"] = "North America";
            countryCodes["DZ"] = "Africa";
            countryCodes["EC"] = "South America";
            countryCodes["EE"] = "Europe";
            countryCodes["EG"] = "Africa";
            countryCodes["EH"] = "Africa";
            countryCodes["ER"] = "Africa";
            countryCodes["ES"] = "Europe";
            countryCodes["ET"] = "Africa";
            countryCodes["FI"] = "Europe";
            countryCodes["FJ"] = "Oceania";
            countryCodes["FK"] = "South America";
            countryCodes["FM"] = "Oceania";
            countryCodes["FO"] = "Europe";
            countryCodes["FR"] = "Europe";
            countryCodes["GA"] = "Africa";
            countryCodes["GB"] = "Europe";
            countryCodes["GD"] = "North America";
            countryCodes["GE"] = "Asia";
            countryCodes["GF"] = "South America";
            countryCodes["GG"] = "Europe";
            countryCodes["GH"] = "Africa";
            countryCodes["GI"] = "Europe";
            countryCodes["GL"] = "North America";
            countryCodes["GM"] = "Africa";
            countryCodes["GN"] = "Africa";
            countryCodes["GP"] = "North America";
            countryCodes["GQ"] = "Africa";
            countryCodes["GR"] = "Europe";
            countryCodes["GS"] = "Antarctica";
            countryCodes["GT"] = "North America";
            countryCodes["GU"] = "Oceania";
            countryCodes["GW"] = "Africa";
            countryCodes["GY"] = "South America";
            countryCodes["HK"] = "Asia";
            countryCodes["HN"] = "North America";
            countryCodes["HR"] = "Europe";
            countryCodes["HT"] = "North America";
            countryCodes["HU"] = "Europe";
            countryCodes["ID"] = "Asia";
            countryCodes["IE"] = "Europe";
            countryCodes["IL"] = "Asia";
            countryCodes["IM"] = "Europe";
            countryCodes["IN"] = "Asia";
            countryCodes["IO"] = "Asia";
            countryCodes["IQ"] = "Asia";
            countryCodes["IR"] = "Asia";
            countryCodes["IS"] = "Europe";
            countryCodes["IT"] = "Europe";
            countryCodes["JE"] = "Europe";
            countryCodes["JM"] = "North America";
            countryCodes["JO"] = "Asia";
            countryCodes["JP"] = "Asia";
            countryCodes["KE"] = "Africa";
            countryCodes["KG"] = "Asia";
            countryCodes["KH"] = "Asia";
            countryCodes["KI"] = "Oceania";
            countryCodes["KM"] = "Africa";
            countryCodes["KN"] = "North America";
            countryCodes["KP"] = "Asia";
            countryCodes["KR"] = "Asia";
            countryCodes["KW"] = "Asia";
            countryCodes["KY"] = "North America";
            countryCodes["KZ"] = "Asia";
            countryCodes["LA"] = "Asia";
            countryCodes["LB"] = "Asia";
            countryCodes["LC"] = "North America";
            countryCodes["LI"] = "Europe";
            countryCodes["LK"] = "Asia";
            countryCodes["LR"] = "Africa";
            countryCodes["LS"] = "Africa";
            countryCodes["LT"] = "Europe";
            countryCodes["LU"] = "Europe";
            countryCodes["LV"] = "Europe";
            countryCodes["LY"] = "Africa";
            countryCodes["MA"] = "Africa";
            countryCodes["MC"] = "Europe";
            countryCodes["MD"] = "Europe";
            countryCodes["ME"] = "Europe";
            countryCodes["MG"] = "Africa";
            countryCodes["MH"] = "Oceania";
            countryCodes["MK"] = "Europe";
            countryCodes["ML"] = "Africa";
            countryCodes["MM"] = "Asia";
            countryCodes["MN"] = "Asia";
            countryCodes["MO"] = "Asia";
            countryCodes["MP"] = "Oceania";
            countryCodes["MQ"] = "North America";
            countryCodes["MR"] = "Africa";
            countryCodes["MS"] = "North America";
            countryCodes["MT"] = "Europe";
            countryCodes["MU"] = "Africa";
            countryCodes["MV"] = "Asia";
            countryCodes["MW"] = "Africa";
            countryCodes["MX"] = "North America";
            countryCodes["MY"] = "Asia";
            countryCodes["MZ"] = "Africa";
            countryCodes["NA"] = "Africa";
            countryCodes["NC"] = "Oceania";
            countryCodes["NE"] = "Africa";
            countryCodes["NF"] = "Oceania";
            countryCodes["NG"] = "Africa";
            countryCodes["NI"] = "North America";
            countryCodes["NL"] = "Europe";
            countryCodes["NO"] = "Europe";
            countryCodes["NP"] = "Asia";
            countryCodes["NR"] = "Oceania";
            countryCodes["NU"] = "Oceania";
            countryCodes["NZ"] = "Oceania";
            countryCodes["OM"] = "Asia";
            countryCodes["PA"] = "North America";
            countryCodes["PE"] = "South America";
            countryCodes["PF"] = "Oceania";
            countryCodes["PG"] = "Oceania";
            countryCodes["PH"] = "Asia";
            countryCodes["PK"] = "Asia";
            countryCodes["PL"] = "Europe";
            countryCodes["PM"] = "North America";
            countryCodes["PN"] = "Oceania";
            countryCodes["PR"] = "North America";
            countryCodes["PS"] = "Asia";
            countryCodes["PT"] = "Europe";
            countryCodes["PW"] = "Oceania";
            countryCodes["PY"] = "South America";
            countryCodes["QA"] = "Asia";
            countryCodes["RE"] = "Africa";
            countryCodes["RO"] = "Europe";
            countryCodes["RS"] = "Europe";
            countryCodes["RU"] = "Europe";
            countryCodes["RW"] = "Africa";
            countryCodes["SA"] = "Asia";
            countryCodes["SB"] = "Oceania";
            countryCodes["SC"] = "Africa";
            countryCodes["SD"] = "Africa";
            countryCodes["SE"] = "Europe";
            countryCodes["SG"] = "Asia";
            countryCodes["SH"] = "Africa";
            countryCodes["SI"] = "Europe";
            countryCodes["SJ"] = "Europe";
            countryCodes["SK"] = "Europe";
            countryCodes["SL"] = "Africa";
            countryCodes["SM"] = "Europe";
            countryCodes["SN"] = "Africa";
            countryCodes["SO"] = "Africa";
            countryCodes["SR"] = "South America";
            countryCodes["ST"] = "Africa";
            countryCodes["SV"] = "North America";
            countryCodes["SY"] = "Asia";
            countryCodes["SZ"] = "Africa";
            countryCodes["TC"] = "North America";
            countryCodes["TD"] = "Africa";
            countryCodes["TF"] = "Antarctica";
            countryCodes["TG"] = "Africa";
            countryCodes["TH"] = "Asia";
            countryCodes["TJ"] = "Asia";
            countryCodes["TK"] = "Oceania";
            countryCodes["TM"] = "Asia";
            countryCodes["TN"] = "Africa";
            countryCodes["TO"] = "Oceania";
            countryCodes["TR"] = "Asia";
            countryCodes["TT"] = "North America";
            countryCodes["TV"] = "Oceania";
            countryCodes["TW"] = "Asia";
            countryCodes["TZ"] = "Africa";
            countryCodes["UA"] = "Europe";
            countryCodes["UG"] = "Africa";
            countryCodes["US"] = "North America";
            countryCodes["UY"] = "South America";
            countryCodes["UZ"] = "Asia";
            countryCodes["VC"] = "North America";
            countryCodes["VE"] = "South America";
            countryCodes["VG"] = "North America";
            countryCodes["VI"] = "North America";
            countryCodes["VN"] = "Asia";
            countryCodes["VU"] = "Oceania";
            countryCodes["WF"] = "Oceania";
            countryCodes["WS"] = "Oceania";
            countryCodes["YE"] = "Asia";
            countryCodes["YT"] = "Africa";
            countryCodes["ZA"] = "Africa";
            countryCodes["ZM"] = "Africa";
            countryCodes["ZW"] = "Africa";
            return countryCodes;
        }
    });
});
