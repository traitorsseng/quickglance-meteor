Stocks = new Meteor.Collection("stocks");
Sports = new Meteor.Collection("sports");
Food = new Meteor.Collection("food");
Users = new Mongo.Collection('useraccounts');

if(Meteor.isClient) {
    Template.dashboard.helpers({
        profilePic: function() {
            return "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/?type=square&height=120&width=120";
        },
        numCols: function() {
            var res = Users.findOne({userID: Meteor.userId()});
            console.log(res);
            var prefs = res.preferences;
            var num = 0;
            for(var i = 0; i < prefs.length; i++) {
                if(prefs[i] == "dining" || prefs[i] == "business" || prefs[i] == "sports"){
                    num++;
                }
            }
            var result = 12/num;
            return result;
        },
    });

    Template.dashboard.events({
        'click #facebook-login-modal-1': function(e) {
            Meteor.loginWithFacebook({}, function(err){
                if (err) {
                    console.log("Login failed");
                    throw new Meteor.Error("Facebook login failed");
                } else {
                    console.log("Login button pressed");
                    Session.set("meteor_userId", Meteor.userId());
                    // get book marks

                    checkChatNotifs();
                    Meteor.call("getFirstLoginStatus", Meteor.userId(), function(error, results) {
                        if(error) {
                            console.log("Error in retrieving login status");
                        } else {
                            console.log(results);
                            if(results == true) {
                                Modal.hide('dashboard');
                                console.log("Showing Tutorial screens");
                                setTimeout(function(){ Modal.show('tutorialBoxType'); }, 500);
                            } else {
                                setTimeout(function(){
                                    createMap();
                                }, 2000);
                                Meteor.call("getBookmarks", Meteor.userId(), getBookmarksCallback());
                                console.log("Not first login");
                            }
                        }
                    });
                }
            });
        },

        'click #google-login-modal-1': function(e) {
            Meteor.loginWithGoogle({}, function(err) {
                if (err) {
                    console.log("Login failed");
                    throw new Meteor.Error("Google login failed");
                } else {
                    console.log("Login button pressed");
                    Session.set("meteor_userId", Meteor.userId());
                    Meteor.call("getFirstLoginStatus", Meteor.userId(), function(error, results) {
                        if(error) {
                            console.log("Error in retrieving login status");
                        } else {
                            console.log(results);
                            if(results == true) {
                                Modal.hide('dashboard');
                                console.log("Showing Tutorial screens");
                                setTimeout(function(){ Modal.show('tutorialBoxType'); }, 500);
                            } else {
                                setTimeout(function(){
                                    createMap();
                                }, 2000);
                                Meteor.call("getBookmarks", Meteor.userId(), getBookmarksCallback());
                                console.log("Not first login");
                            }
                        }
                    });
                }
            });
        },

    });

    Template.dashButton.events({
        'click .dashbtn': function(e) {
            Modal.show('dashboard');
        }
    });

    Template.recommendations.events({
        'click .toparticle': function(e) {
            console.log("User clicks recommended article");
            Modal.hide('dashboard');
            var image = "";
            if (typeof this.multimedia != "undefined" && this.multimedia != ""){
                image = this.multimedia[1].url;
            } else if (typeof this.media != "undefined" && this.media != "") {
                //console.log(article);
                image = this.media[0]['media-metadata'][1].url;
            }
            var contentString = '<h4>' + this.title + '</h4><p>' + this.abstract + "</p><img class='img-thumbnail' src='" + image + "'></img>";
            document.getElementById('news-summary').innerHTML = contentString;
            Session.set('article_id', this.title);
            Session.set('article_url', this.url);
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("navbarID").style.marginLeft = "200px";
            document.getElementById("searchResultsBar").style.width = "0px";
        },
    });

    Template.voiceSearchBig.events({
        'click .speechRecDash': function(event) {
            console.log("Pressed Speech Recognition Button");

            var html5api = new Html5Api();
            var speech = html5api.speechRecognition();

            if(speech) {
                Session.set("typeOfNews", "");
                Modal.hide('dashboard');
                document.getElementById('speechButton').innerHTML = "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>";
                document.getElementById("searchResultsBar").style.width = "300px";
                document.getElementById("navbarID").style.marginLeft = "0px";
                document.getElementById("mySidenav").style.width = "0px";
                // document.getElementById("main").style.marginLeft = "-250px";
                speech.start(function (err, res) {
                    if (err) console.log(err);
                    else {
                        document.getElementById("searchTerm").innerHTML = res.text;
                        console.log(res);
                        //document.getElementById('speechButton').innerHTML = res.text;
                        Session.set("speechResult", res.text);
                        Session.set("display_results", false);
                        Meteor.setTimeout(function () {
                            speech.stop();
                            document.getElementById('speechButton').innerHTML = "<i class='fa fa-microphone' aria-hidden='true'></i>";
                            Session.set("typeOfNews", Session.get("speechResult"));
                            //Modal.show("searchModal");
                            Meteor.setTimeout(function() {
                                Session.set("display_results", true);
                            }, 3000);
                        }, 3000);
                    }
                });
            }
            Meteor.setTimeout(function () {
                console.log("Final recording: " + Session.get("speechResult"));

                console.log("Finished Speech recording");
            }, 6000);
        },

        'keyup input.searchBarBig': function(event, template) {
            if (event.which === 13) {
                var searchTerm = template.find(".searchBarBig").value;
                console.log("Search Bar Enter Pressed " + searchTerm);
                                 Meteor.setTimeout(function() {
                                                   Session.set("display_results", true);
                                                   }, 3000);
                Session.set("typeOfNews", searchTerm);
                Modal.hide('dashboard');
                document.getElementById("searchResultsBar").style.width = "300px";
                document.getElementById("navbarID").style.marginLeft = "0px";
                document.getElementById("mySidenav").style.width = "0px";
            }
        },

        'click .btn-search': function(event, template) {
            var searchTerm = template.find(".searchBarBig").value;

            console.log("Search Bar Enter Pressed " + searchTerm);
                             Meteor.setTimeout(function() {
                                               Session.set("display_results", true);
                                               }, 3000);
            Session.set("typeOfNews", searchTerm);
            Modal.hide('dashboard');
            document.getElementById("searchResultsBar").style.width = "300px";
            document.getElementById("navbarID").style.marginLeft = "0px";
            document.getElementById("mySidenav").style.width = "0px";
        }
    })
}
