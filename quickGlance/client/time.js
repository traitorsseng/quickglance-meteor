if(Meteor.isClient) {
    Template.fpanel.helpers({
        getTime() {
            var currentHour = moment().format("HH");
            var greeting;
            if(currentHour >= 12 && currentHour <= 17) {
                g = "Afternoon";
            } else if(currentHour >= 17) {
                g = "Evening";
            } else {
                g = "Morning";
            }
            return g;
        },
    });
}
