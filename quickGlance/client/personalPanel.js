Template.personalPanels.helpers({
    stocks: function() {
        var stockInfo = Stocks.find({}).fetch();
        console.log("Getting Stock Info");
        console.log(stockInfo);
        var array = [];
        for(var i = 0; i < 20; i++) {
            array.push(stockInfo[0][i]);
        }
        console.log(array);
        return array;
    },
    sports: function(){
        var sportInfo = Sports.find({}).fetch();
        console.log("Getting Sports Info...");
        console.log(sportInfo[0][0]);
        var obj = sportInfo[0][0];
        var array = [];
        array = obj.matches;
        return array;
    },
    foods: function() {
        var lat = Session.get("lat");
        var lon = Session.get("lon");
        if(lat == "") {
            lat = "-33.868820";
        }
        if(lon == "") {
            lon = "151.209296";
        }
        var result = ReactiveMethod.call('getFoodPlaces', lat, lon);
        console.log(result);
        var array = [];
        if(typeof result != "undefined") {
            array = result.results;
        }
        //array = Session.get("arrayOfFood");
        return array;
    },
    chosenFood: function() {
        var res = Users.findOne({userID: Meteor.userId()});
        console.log(res);
        var prefs = res.preferences;
        for(var i = 0; i < prefs.length; i++) {
            if(prefs[i] == "dining"){
                return true;
            }
        }
        return false;
    },
    chosenSport: function(){
        var res = Users.findOne({userID: Meteor.userId()});
        var prefs = res.preferences;
        for(var i = 0; i < prefs.length; i++) {
            if(prefs[i] == "sports"){
                return true;
            }
        }
        return false;
    },
    chosenBusiness: function() {
        var res = Users.findOne({userID: Meteor.userId()});
        var prefs = res.preferences;
        for(var i = 0; i < prefs.length; i++) {
            console.log(prefs[i]);
            if(prefs[i] == "business"){
                return true;
            }
        }
        return false;
    },

    stockchange: function() {
        // console.log("Stock Change: ");
        // console.log(this.change);
        if(this.change < 0) {
            return "bottom";
        } else {
            return "top";
        }
    },
    stockcolour: function() {
        // console.log("Stock Change: ");
        // console.log(this.change);
        if(this.change < 0) {
            return "redStock";
        } else {
            return "greenStock";
        }
    },
});

Template.recommendations.helpers({
    nonChosen: function() {
        var res = Users.findOne({userID: Meteor.userId()});
        console.log(res);
        var prefs = res.preferences;
        var count = 0;
        for(var i = 0; i < prefs.length; i++) {
            if(prefs[i] == "dining" || prefs[i] == "business" || prefs[i] == "sports"){
                count++;
            }
        }
        if(count == 0) {
            return true;
        } else {
            return false;
        }
    }
});
