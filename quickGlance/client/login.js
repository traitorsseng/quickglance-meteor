if(Meteor.isClient) {
    Meteor.startup(function(){
        //If user hasn't logged on
        if(!Meteor.userId()){
            console.log("User not logged in");
            setTimeout(function(){ Modal.show('loginBox') }, 500);
        }
    });

    //Login and logout buttons in the navbar
    Template.body.events({
        'click #facebook-login': function(e) {
            Modal.show('loginBox');
        },

        'click #logout': function(e) {
            Meteor.logout(function(err){
                if (err) {
                    throw new Meteor.Error("Logout failed");
                } else {
                    removeAllMarkers();
                    setZoomQuickGlance(3);
                    centerContinent(0,0);
                    Session.set('numNotifs', "");
                    Session.set('numBookmarks', "");
                    Meteor.call("updateBookmarks", Session.get("meteor_userId"), Bookmarks.find().fetch());
                    Meteor.call("updateLocationsToZoom", Session.get("meteor_userId"), Session.get('locationZoom'));
                    Bookmarks.remove({});
                    console.log("Sent some info to server");
                    setTimeout(function(){ Modal.show('loginBox') }, 1000);
                }
            });
        }
    });

    //Login button in the modal popup
    Template.loginBox.events({
        'click #facebook-login-modal': function(e) {
            Meteor.loginWithFacebook({}, function(err){
                if (err) {
                    console.log("Login failed");
                    throw new Meteor.Error("Facebook login failed");
                } else {
                    console.log("Login button pressed");
                    Session.set("meteor_userId", Meteor.userId());
                    // get book marks
                    Modal.hide('loginBox');
                    checkChatNotifs();
                    Meteor.call("getFirstLoginStatus", Meteor.userId(), function(error, results) {
                        if(error) {
                            console.log("Error in retrieving login status");
                        } else {
                            console.log(results);
                            if(results == true) {
                                console.log("Showing Tutorial screens");
                                setTimeout(function(){ Modal.show('tutorialBoxType'); }, 500);
                            } else {
                                setTimeout(function(){
                                    Modal.show('dashboard');
                                    createMap();
                                }, 2000);
                                Meteor.call("getBookmarks", Meteor.userId(), getBookmarksCallback());
                                console.log("Not first login");
                            }
                        }
                    });
                }
            });
        },

        'click #google-login-modal': function(e) {
            Meteor.loginWithGoogle({}, function(err) {
                if (err) {
                    console.log("Login failed");
                    throw new Meteor.Error("Google login failed");
                } else {
                    console.log("Login button pressed");
                    Session.set("meteor_userId", Meteor.userId());
                    Modal.hide('loginBox');
                    Meteor.call("getFirstLoginStatus", Meteor.userId(), function(error, results) {
                        if(error) {
                            console.log("Error in retrieving login status");
                        } else {
                            console.log(results);
                            if(results == true) {
                                console.log("Showing Tutorial screens");
                                setTimeout(function(){ Modal.show('tutorialBoxType'); }, 500);
                            } else {
                                setTimeout(function(){
                                    Modal.show('dashboard');
                                    createMap();
                                }, 2000);
                                Meteor.call("getBookmarks", Meteor.userId(), getBookmarksCallback());
                                console.log("Not first login");
                            }
                        }
                    });
                }
            });
        },

    });

    //Next Button in tutorial box selecting type of newUser
    Template.tutorialBoxType.events ({
        'submit #type-form-tutorial': function(e, template) {
            console.log("Submitted tutorial type form");
            e.preventDefault();
            var selected = template.findAll("input[type=checkbox]:checked");
            var array = _.map(selected, function(item) {
                return item.defaultValue;
            });
            console.log("Selected types: " + array);
            for(var i = 0; i < array.length;i++) {
                var currentPref = array[i];
                document.getElementById(currentPref).checked = true;
            }
            // Meteor.call("getPreferences", Meteor.userId(), addUser(array));
            Meteor.call("doesUserExist", Meteor.userId(), function(err, results){
                if (err){
                    console.log("Error checking whether user actually exists");
                }

                else{
                    var userExists = results;
                    // user doesn't exists so create a new usr object to store on the server
                    if (!userExists){
                        var newUserData = {
                            userID: Meteor.userId(),
                            preferences: array,
                            commentNotifications: [], // I reckon this should just hold the urls
                            // unreadArticles: [], // not doing this
                            locationToZoom: "", // the continent that the user was last zoomed into
                            bookmarkedArticles: [], // same with this
                            subscribedArticles: [],
                            interestedTopics: [],
                            //suggestedArticles: [],
                            firstTimeLogin: true
                        };

                        Meteor.call("newUser", Meteor.userId(), newUserData);
                        Meteor.call("changeLoginStatus", Meteor.userId());
                    }
                }
            });

            Modal.hide('tutorialBoxType');
            setTimeout(function(){ Modal.show('tutorialBoxLocation'); }, 500);
        }
    });

    //Submit button for Location of news
    Template.tutorialBoxLocation.events ({
        'submit #location-form-tutorial': function(e, template) {
            console.log("Submitted Location type form");
            e.preventDefault();
            var selected = template.find("input[type=radio]:checked");
            var continent = selected.value;
            setContinentZoom(continent);
            // displayForContinent();

            // set the radio button in the dropdown
            var radioButton = document.getElementById(continent);
            radioButton.checked = true;

            //  FOR MATT : Update locations of user in the database
            Session.set('locationZoom', continent);
            console.log("Selected locations: " + selected.value);
            Modal.hide('tutorialBoxLocation');
            setTimeout(function(){
                Modal.show('dashboard');
                createMap();
            }, 2000);
        },
        'click .backbttn':function() {
            Modal.hide('tutorialBoxLocation');
            setTimeout(function(){ Modal.show('tutorialBoxType'); }, 500);
        }

    });

}

getBookmarksCallback = function(){
    var returnFunction = function(err, results){
        if (err){
            console.log("Error acquiring user bookmarks");
        }

        else{
            var userBookmarksServer = results;
            console.log("[CLIENT START UP]: Getting bookmarks");
            for (var i = 0; i < userBookmarksServer.length; i++){
                // userBookmarksServer[i]._id = new Meteor.Collection.ObjectID();
                var bookmark = userBookmarksServer[i];
                var exists = Bookmarks.find({_id: bookmark._id}).fetch();
                console.log("Bookmark: " + bookmark + " Exists = " + exists[0]);
                if (typeof exists[0] == "undefined"){
                    // if (exists[0]._id != bookmark._id){
                    console.log("Adding article to database of bookmarks");
                    Bookmarks.insert(bookmark);
                    //}
                }

                else{
                    console.log("Don't want to add duplicates");
                }
            }
        }
    };

    return returnFunction;
}
