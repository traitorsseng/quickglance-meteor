import sys
from bs4 import BeautifulSoup
from bs4.diagnose import diagnose
import requests
import re

def get_text(url):
        data=""
        p=requests.get(url).content
        soup=BeautifulSoup(p,"html.parser")
        data=p
        geolocation = soup.find(attrs={'name':'geo.position'})
        return str(geolocation)

url = sys.argv[1]

result = get_text(url)
result = re.sub(r'<meta content="', "", result)
result = re.sub(r'" name="geo.position"/>', "", result)

print(result.rstrip())
