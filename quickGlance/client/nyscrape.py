import sys
from bs4 import BeautifulSoup
from bs4.diagnose import diagnose
import requests
import re

def get_text(url):
        data=""
        p=requests.get(url).content
        soup=BeautifulSoup(p,"html.parser")    
        paragraphs=soup.select("p.story-body-text.story-content")
        data=p
        text=""
        for paragraph in paragraphs:
            text += "\n" + paragraph.text
        text=text.encode('ascii', 'ignore')
        return str(text)

url = sys.argv[1]

result = get_text(url)
result = re.sub(r'b\' ', "", result) 
result = re.sub(r'\'\s*', "", result) 

print(result)

