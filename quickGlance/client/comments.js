// Displaying comments in chronological order

if(Meteor.isClient){
    Template.comments.helpers({
        'commentMessage' : function() {
            return Comments.find({a_id: Session.get('article_id')}, {
                sort: {timestamp: 1},
                limit: 20
            });
        }
    });
}
