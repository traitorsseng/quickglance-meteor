import sys
from bs4 import BeautifulSoup
from bs4.diagnose import diagnose
import requests
import re

def get_text(url):
        data=""
        p=requests.get(url).content
        soup=BeautifulSoup(p,"html.parser")
        paragraphs=soup.select("p.story-body-text.story-content")
        data=p
        text=" <div class=\"news-body\""
        for paragraph in paragraphs:
            text += "    " + paragraph.text
        text=text.encode('ascii', 'ignore')
        imgs = soup.findAll("div",{"class":"image"})
        for img in imgs:
            print ("<img class=\"news-img\" float=”right” HEIGHT=\"55%\" WIDTH=\"55%\" src=")
            print (img.img['src'])
            print (">")
        return str(text)

url = sys.argv[1]

result = get_text(url)
result = re.sub(r'b\' ', "", result)
rseult = re.sub(r'<\\\\div>',"",result)
result = re.sub(r'\'\s*', "", result)
result = re.sub(r'    ', "<br><br>", result)

print(result)
