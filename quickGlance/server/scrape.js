if(Meteor.isServer) {
    Meteor.methods({
        callPy: function(url){
            var childProcess = Meteor.npmRequire("child_process");
            var Fiber = Meteor.npmRequire('fibers');
            var Future = Npm.require('fibers/future');

            var fut = new Future();
            new Fiber(function() {
                console.log('test python file');
                var file_path = Meteor.absolutePath + "/public/scripts/nyscrape.py";
                console.log("Script file path: " + file_path);
                //var url = "http://www.nytimes.com/2016/10/08/opinion/the-sleaziness-of-donald-trump.html"
                childProcess.exec("python3 " + file_path + " " + url, function(error, stdout, stderr) {
                    if (error) console.log(error);
                    if (stdout) {
                        console.log(stdout);
                        fut.return(stdout);
                    }
                    if (stderr) console.log(stderr);
                });
            }).run();
            //console.log("hi");
            //console.log(file.toString());
            return fut.wait();

        },

        pyGeoloc: function(url){
            var childProcess = Meteor.npmRequire("child_process");
            var Fiber = Meteor.npmRequire('fibers');
            var Future = Npm.require('fibers/future');

            var fut = new Future();
            new Fiber(function() {
                console.log('test geolocation python file');
                var file_path = Meteor.absolutePath + "/public/scripts/scrapegeo.py";
                console.log("Script file path: " + file_path);
                // var url = "http://www.abc.net.au/news/2016-10-20/200k-jobs-at-risk-as-housing-boom-rolls-over:-morgan-stanley/7952478";
                childProcess.exec("python3 " + file_path + " " + url, function(error, stdout, stderr) {
                    if (error) console.log(error);
                    if (stdout) {
                        console.log(stdout);
                        fut.return(stdout);
                    }
                    if (stderr) console.log(stderr);
                });
            }).run();
            //console.log("hi");
            //console.log(file.toString());
            return fut.wait();

        },

        scrapeOther: function(url){
           data = Scrape.website(url);
           return data;
        }
    });
}
