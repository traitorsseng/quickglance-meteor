Articles = new Mongo.Collection("articles");
Locations = new Mongo.Collection("locations");
Continents = new Mongo.Collection(null);

/*
    Server:
    * Store all the data that the client needs and just send to the client so the client doesn't have to do any downloading
*/

Meteor.methods({
    getNews: function(type_of_news) {
        // this.unblock();
        var url = "https://api.nytimes.com/svc/topstories/v1/";
        url += type_of_news + ".json?api-key=6f0746398a654619bf73cad6c67714cd";
        // Articles.remove({}); // clear the data base
        var result = HTTP.call('GET', url); // get the JSON object from NY times
        var results = JSON.parse(result.content); // probably needs some error checking
        var articles_raw = results.results;

        if(type_of_news == "world") {
            console.log("Getting Most Popular World News");
            var url1 = "https://api.nytimes.com/svc/mostpopular/v2/mostshared/";
            url1 += type_of_news + "/7.json?api-key=6f0746398a654619bf73cad6c67714cd";
            var result1 = HTTP.call('GET', url1);
            var results1 = JSON.parse(result1.content);
            articles_raw = articles_raw.concat(results1.results);
        }

        var events = {
            typeNews: type_of_news,
            articles: articles_raw
        };

        Articles.insert(events);
               //console.log(articles_raw);
        // console.log("Results: " + results);
    },

    searchNews: function(q) {
        // Built by LucyBot. www.lucybot.com
        var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
        url += "?api-key=09e648e6fae04577812974596751fc24&q=" + q + "&begin-date=20161018";
        var result = HTTP.call('GET', url); // get the JSON object from NY times
        var results = JSON.parse(result.content); // probably needs some error checking
        console.log(results);
        return results;
    },

    storeNewsLocations: function(){
        var events = Articles.find().fetch(); // retrieve all the Articles
        var key = "AkVnhoCSuHBuwaL0Pvzpksx813bPMF_5Z1erzMigV3PICNy12hfx4R9B4phkDn-n";
        // Locations.remove({});
        // console.log(events);
        // extract the locations
        for (var k = 0; k < events.length; k++){
            nyTimesArticles = events[k].articles;
            typeOfNews = events[k].typeNews;
            var geocodes = [];
            // geocode all the articles
            for (var i = 0; i < nyTimesArticles.length; i++){
                var article = nyTimesArticles[i];
                var parsedLocationInfo = "";

                // try all the locations in the geo_facet array
                for (var j = 0; j < article.geo_facet.length; j++){
                    var location = article.geo_facet[j];
                    // get the country code as well
                    var url = "http://dev.virtualearth.net/REST/v1/Locations?query=" + location + "&incl=ciso2&output=json&key=" + key;
                    var locationInfo = HTTP.call('GET', url);
                    parsedLocationInfo = JSON.parse(locationInfo.content);
                    // console.log("Country code = " + parsedLocationInfo.resourceSets[0].resources[0].address.countryRegionIso2);
                    if (typeof parsedLocationInfo != "undefined" && typeof parsedLocationInfo.resourceSets != "undefined" && parsedLocationInfo.resourceSets.length > 0 &&
                        typeof parsedLocationInfo.resourceSets[0] != "undefined" && typeof parsedLocationInfo.resourceSets[0].resources != "undefined"
                        && parsedLocationInfo.resourceSets[0].resources.length > 0 &&
                            typeof parsedLocationInfo.resourceSets[0].resources[0] != "undefined"){
                        // console.log("The location is: " + location);
                        // console.log(parsedLocationInfo.resourceSets[0].resources[0]);
                        break;
                    }
                }

                if (typeof parsedLocationInfo != "undefined" && parsedLocationInfo != "" && typeof parsedLocationInfo.resourceSets != "undefined" && parsedLocationInfo.resourceSets.length > 0 &&
                    typeof parsedLocationInfo.resourceSets[0] != "undefined" && typeof parsedLocationInfo.resourceSets[0].resources != "undefined"
                    && parsedLocationInfo.resourceSets[0].resources.length > 0 &&
                        typeof parsedLocationInfo.resourceSets[0].resources[0] != "undefined"){
                    var latitude = parsedLocationInfo.resourceSets[0].resources[0].point.coordinates[0]; // get the latitude
                    var longitude = parsedLocationInfo.resourceSets[0].resources[0].point.coordinates[1];  // get the longitude
                    var country_code = parsedLocationInfo.resourceSets[0].resources[0].address.countryRegionIso2;
                    var LatLng = {
                        articleBelongTo: article,
                        lat: latitude,
                        lon: longitude,
                        countryCode: country_code
                    };

                    geocodes.push(LatLng); // add all the geocoding info to the array of latitude and longitudes
                }
            }

            var locations = {
                typeNews: typeOfNews,
                points: geocodes
            };

            Locations.insert(locations);
        }
    },

	getNewsLocations: function(typeOfNews){
        var newsLocations = Locations.find().fetch();
        // console.log(newsLocations[0].points);
        var locationInfo;
        for (var i = 0; i < newsLocations.length; i++){
            if (newsLocations[i].typeNews == typeOfNews){
                locationInfo = newsLocations[i].points;
                break;
            }
        }

        return locationInfo;
    },

    // gets the geo data for each continent
    storeContinentInfo: function(){
        var continents = ["Africa", "South America", "Oceania", "Europe", "Asia", "North America", "Antarctica"];
        var key = "AkVnhoCSuHBuwaL0Pvzpksx813bPMF_5Z1erzMigV3PICNy12hfx4R9B4phkDn-n";

        for (var i = 0; i < continents.length; i++){
            var url = "http://dev.virtualearth.net/REST/v1/Locations?query=" + continents[i] + "&incl=ciso2&output=json&key=" + key;
            var locationInfo = HTTP.call('GET', url);
            var parsedLocationInfo = JSON.parse(locationInfo.content);
            if (typeof parsedLocationInfo.resourceSets[0].resources[0] != "undefined"){
                var latitude = parsedLocationInfo.resourceSets[0].resources[0].point.coordinates[0]; // get the latitude
                var longitude = parsedLocationInfo.resourceSets[0].resources[0].point.coordinates[1];  // get the longitude
                // console.log(continents[i] + " lat = " + latitude + " lon = " + longitude);
                var contData = {
                    name: continents[i],
                    lat: latitude,
                    lon: longitude
                };

                Continents.insert(contData);
            }
        }
    },

    // retrieve the data for continents
    getContinentsInfo: function(){
        var dataForContinents = Continents.find().fetch();
        return dataForContinents;
    },
});
