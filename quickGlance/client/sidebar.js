if(Meteor.isClient){
    Template.sideBar.events({
        'click .openBar': function(e) {
            e.preventDefault();
            console.log("Button Pressed");
            document.getElementById("mySidenav").style.width = "250px";
        },

        'click .read-button':function(e) {
            e.preventDefault();
            if(Meteor.userId()){
                console.log("Read More Button Pressed: ");
                console.log(Session.get("keywords"));
                Meteor.call("addToKeywords", Meteor.userId(), Session.get("keywords"));
                Meteor.call("findArticlesWithKeywords", Meteor.userId(), function(err, results) {
                    if(err) {
                        console.log("Error getting keywords");
                    } else {
                        console.log("Results for suggested articles: ");
                        console.log(results);
                    }
                });
            }
        },

        'click .closeBar': function(e) {
            e.preventDefault();
            console.log("Button Pressed");
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("navbarID").style.marginLeft = "120px";
        },

        // user wants to bookmark an article
        'click .bookmark': function(e){
            console.log("User wants to bookmark this article.\n");
            // check if the article title and the url have been set
            var articleTitle = Session.get('article_id');
            var articleUrl = Session.get('article_url');

            // if one of these is undefined, a marker was probably not clicked
            if (typeof articleTitle != "undefined" && typeof articleUrl != "undefined"){
                var bookmark = {
                    article_title: articleTitle,
                    article_url: articleUrl
                };

                // only add to the collection of bookmarks if the bookmark is not already there
                var bookmarkObject = Bookmarks.find({article_title: articleTitle}).fetch();
                console.log("bookmarkObject: " + bookmarkObject);
                if (typeof bookmarkObject == "undefined" || bookmarkObject == ""){
                    Bookmarks.insert(bookmark);
                }

                else{
                    sAlert.error("You have already bookmarked this article", {timeout: 5000, html: true});
                }
            }

            else{
                console.log("Error sending new bookmark to server.\n");
            }
        }
    });

    Template.readmore.helpers({
        'timerEnded': function(){
            var loadBool = Session.get('keep_loading');
            if (loadBool != false){
                loadBool = true;
            }
            console.log("loadBool = " + loadBool);
            return loadBool;
        },
        title(){
            return Session.get('article_id');
        },
    });
}
