if(Meteor.isServer) {
    Meteor.startup(function () {
        // code to run on server at startup
    });

    Meteor.methods({
        // The method expects a valid US zip code
        "weather": function (lat, lon) {
            console.log('Get weather for' + lat + " , " + lon);
            // Construct the API URL
            var apiKey = "10053e58fff8038781f5eb2f534564d5";
            var apiUrl = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&APPID=" + apiKey;
            // query the API
            var response = HTTP.get(apiUrl).data;
            return response;
        }
    });
}
