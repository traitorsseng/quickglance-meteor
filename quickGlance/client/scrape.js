if(Meteor.isClient) {
    Template.sideBar.events({
        'click .read-button': function(e) {
            Modal.show("readmore");
            var url = Session.get("article_url");
            Session.set('keep_loading', true);
            // ABC article so must use a different scraper
            if (/www.abc.net.au/.test(url)){
                Meteor.call('scrapeOther', url, function(err, result) {
                    if(err) {
                        console.log("Error scraping bing news article");
                    } else {
                        var resultString = "";
                        Meteor.setTimeout(function(){
                            console.log(result);
                            Session.set('keep_loading', false);
                            console.log("Setting the scrape's false first");
                            var text = result.text;
                            var newText = text.match(/\(?[^\.\?\!]+[\.!\?]\)?/g);
                            console.log(newText.length);
                            for(var i = 0; i < newText.length; i++) {
                                if(i != 0 && i % 5 == 0) {
                                    resultString += "<br><br>" + newText[i];
                                } else {
                                    resultString += " " + newText[i];
                                }
                            }
                        }, 100);

                        Meteor.setTimeout(function(){
                            var content = "<img class='news-img' float='right' HEIGHT='55%' WIDTH='55%' src='" + result.image + "'>";
                            content += "<div class='news-body'>" + resultString +"</div>";
                            document.getElementById("news-article").innerHTML = content;
                        }, 300);
                    }
                });
            }

            else{
                Meteor.call('callPy', url, function(err, results){
                    if(err) {
                        console.log("Error retrieving scraped article");
                    } else {
                        console.log(results);
                        Session.set('keep_loading', false);
                        console.log("Setting the scrape's false first");

                        // wait a 100ms to allow the loading screen to be destroyed
                        Meteor.setTimeout(function(){
                            document.getElementById("news-article").innerHTML = results;
                        }, 100);
                    }
                });
            }
        },
    });

    Template.recommendations.events({
        'click .suggarticle' : function(e) {
            //e.preventDefault();
            console.log("Clicked Suggested Article");
            console.log(this.url);
            Session.set("article_id", this.name);
            Modal.hide("dashboard");
            Meteor.setTimeout(function() {
                Modal.show("readmore");
            }, 500);
            Session.set('keep_loading', true);
            Meteor.call('scrapeOther', this.url, function(err, result) {
                if(err) {
                    console.log("Error scraping bing news article");
                } else {
                    var resultString = "";
                    Meteor.setTimeout(function(){
                        console.log(result);
                        Session.set('keep_loading', false);
                        console.log("Setting the scrape's false first");
                        var text = result.text;
                        var newText = text.match(/\(?[^\.\?\!]+[\.!\?]\)?/g);
                        console.log(newText.length);
                        for(var i = 0; i < newText.length; i++) {
                            if(i != 0 && i % 5 == 0) {
                                resultString += "<br><br>" + newText[i];
                            } else {
                                resultString += " " + newText[i];
                            }
                        }
                    }, 100);

                    Meteor.setTimeout(function(){
                        var content = "<img class='news-img' float='right' HEIGHT='55%' WIDTH='55%' src='" + result.image + "'>";
                        content += "<div class='news-body'>" + resultString +"</div>";
                        document.getElementById("news-article").innerHTML = content;
                    }, 300);
                }
            });
            //Meteor.call('scrapeOther', )
        },
    });
}
