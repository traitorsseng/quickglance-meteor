AllArticles = new Mongo.Collection("allArticles");
// Just add it to the same Articles Collection
Meteor.methods ({
    // this is cool but has no geo-location
    getTechCrunchNews: function() {
        var url = "https://newsapi.org/v1/articles?source=techcrunch&apiKey=8fc912c59643440c84022a720816f02c";
        var result = HTTP.call('GET', url);
        var results = JSON.parse(result.content);
        console.log("Tech Crunch News: " + results);
        AllArticles.insert(results);
    },

    getABCNews: function(){
        // get the exisiting world objects in location collection
        var world_locations = Locations.find({typeNews: "world"}).fetch();
        var existing_world_objects = world_locations[0].points;

        // console.log(world_locations
        // get the ABC news articles
        var url = "https://newsapi.org/v1/articles?source=abc-news-au&sortBy=top&apiKey=8fc912c59643440c84022a720816f02c";
        var result = HTTP.call('GET', url);
        var results = JSON.parse(result.content);
        console.log("ABC News: ");
        console.log(results.articles);

        // convert ABC news article objects to a format similar to NYTIMES
        var abc_converted = format_abc_news(results.articles);

        // join abc_converted with exisiting points
        var world_joined = existing_world_objects.concat(abc_converted);

        // update the existing world news objects in locations with the ABC additions
        var world_geo_id = world_locations[0]._id;
        Locations.update({_id: world_geo_id}, { $set: {points: world_joined} });
    }
});

// format the ABC articles to a format similar to NYTIMES
function format_abc_news(abc_articles){
    var converted_abc_news = [];
    // loop through all the abc articles
    for (var i = 0; i < abc_articles.length; i++){
        var article = abc_articles[i];
        var title = article.title;
        var abstract = article.description;
        var image = article.urlToImage;
        var url = article.url;
        var geo_position = "";
        console.log("Url = " + url);
        url = url.replace(/'/g, "\\'");
        console.log("Regexed url = " + url);
        Meteor.call("pyGeoloc", url, function(err, results){
            if (err){
                console.log("Error calling script to extract geo-positions from ABC articles");
            }

            else{
                geo_position = results;
            }
        });
        console.log("[ABC article geo-positions]:");
        console.log(geo_position);
        console.log("[END OF ABC geo-positions]");

        // Need to use regex to extract lat and lon
        var latitude = 0;
        var longitude = 0;
        var match = /([^\;]*);([^\;]*)/.exec(geo_position);
        if (match != null){
            latitude = parseFloat(match[1]);
            longitude = parseFloat(match[2]);
            //longitude.replace(/(\r\n|\n|\r)/gm,"");
            console.log("Latitude = " + latitude + " longitude = " + longitude);
        }
        //console.log("hello");
        // dummy values to match nytimes article object format
        var geo = [];
        var images = [];
        var dummy = {
            url: image
        };

        // something hacky to avoid big changes in client code particularly in map.js
        geo[0] = "";
        images[1] = dummy;

        // transform ABC => NY Times
        var article_object_ny = {
            title: title,
            abstract: abstract,
            url: url,
            geo_facet: geo,
            multimedia: images
        };

        // prepare to store this straight into converted_abc_news
        var LatLng = {
            articleBelongTo: article_object_ny,
            lat: latitude,
            lon: longitude,
            countryCode: "AU"
        };

        converted_abc_news.push(LatLng);
    }

    return converted_abc_news;
}
