if(Meteor.isClient) {
    Template.fpanel.helpers({
        location: function () {
            return Session.get("city");
        },
        description: function () {
            return Session.get("description");
        },
        temp: function () {
            return Session.get("temperature");

        },
        icon: function () {
            var iconUrl = "http://openweathermap.org/img/w/" + Session.get("icon") + ".png";
            return iconUrl;
        },
        getWeather: function() {
            var html5api = new Html5Api();
            var geoLocation = html5api.geoLocation();
            if (geoLocation) {
                geoLocation.getLocation(function(err, res){
                    if (err) console.log(err);
                    else {
                        //console.log(res);
                        var lat = res.latitude;
                        var lon = res.longitude;
                        Session.set("lat", lat);
                        Session.set("lon", lon);
                        console.log("latitude : " + lat);
                        Meteor.call("weather", lat, lon, function(err,res){
                            console.log(res);
                            Session.set("city", res.name);
                            Session.set("temperature", (Math.round(res.main.temp - 273.15)) + "°C");
                            Session.set("description", res.weather[0].description);
                            Session.set("icon", res.weather[0].icon);
                        });
                    }
                });
            }
        }
    });
}
