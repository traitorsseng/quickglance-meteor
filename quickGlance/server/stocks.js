Stocks = new Meteor.Collection("stocks");

if(Meteor.isServer) {
    Meteor.methods({
        getStockInfo : function() {
            Stocks.remove({});
            var snapshot = YahooFinance.snapshot({
                symbols: [
                  'AAPL',
                  'MQG.AX',
                  'GOOG',
                  'MSFT',
                  'IBM',
                  'AMZN',
                  'ORCL',
                  'INTC',
                  'QCOM',
                  'FB',
                  'CSCO',
                  'SAP',
                  'TSM',
                  'BIDU',
                  'EMC',
                  'HPQ',
                  'TXN',
                  'ERIC',
                  'ASML',
                  'CAJ',
                  'YHOO'
                ],
                fields:['s','n', 'l1', 'c1']});
            Stocks.insert(snapshot);
        },
    })
}
